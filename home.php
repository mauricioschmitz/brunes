<?php
require 'path.php';

require 'include_share/class.textos.php';
require 'include_share/class.banners.php';
require 'include_share/class.noticias.php';

$PAGE = new body();
$PAGE_TEXTOS = new textos();
$PAGE_BANNERS = new banners();
$PAGE_NOTICIAS = new noticias();

$PAGE->conecta_banco();

//parametros para otimização do google
$parametros['title'] = TITULO;
$parametros['keywords'] = "";
$parametros['description'] = "";
$PAGE->imprime_cabecalho_site($parametros, URLSITE);
?>

<div class="container-fluid">  
    <?=$PAGE->imprime_topo('home');?>
    <?=$PAGE_BANNERS->mostra_banners();?> 
    <section id="welcome">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1 welcomeContent">
            	<div class="row">
                	<div class="col-md-3 col-sm-12 col-xs-12 brunes">
            			<img src="imagens/brunes.png" />
                    </div>
                    <div class="col-md-offset-3 col-md-9 col-sm-12 col-xs-12 text">
            			<h1>BEM VINDO!</h1>
                        <p><?php echo $PAGE_TEXTOS->mostra_texto('bemvindo');?></p>
                    </div>
                </div>
            </div>	 
		</div>            
	</section>  
    <section id='events'>
        <div class="container">
            <div class="row">
            	<div class="col-md-offset-1 col-md-10">
                	<div class="aligncenter"><h2 class="aligncenter">&Uacute;ltimas Not&iacute;cias</h2><?php echo $PAGE_TEXTOS->mostra_texto('noticias');?><br />
					</div>
                    <br>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-offset-1 col-md-10">
                	<?php $PAGE_NOTICIAS->mostra_home();?>
                </div>
            </div>
        </div>
    </section> 
    
    <?php echo $PAGE->rodape_site();?>
</div>

<?php
//$PAGE->imprime_topo('home');
/*
?>
  <div id="banners"><?=$PAGE_BANNERS->mostra_banners();?></div>
  <div class="clear"></div>
  <div style="height:450px;"></div>
<? $PAGE->imprime_rodape(true); */?>