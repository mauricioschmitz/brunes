<?php
require 'path.php';

require 'include_share/class.contatos.php';

$PAGE = new body();
$PAGE_CONTATOS = new contatos();

$PAGE->conecta_banco();

//parametros para otimiza��o do google
$parametros['title'] = TITULO;
$parametros['keywords'] = "";
$parametros['description'] = "";
$PAGE->imprime_cabecalho_site($parametros, URLSITE);
?>

<div class="container-fluid">  
    <?=$PAGE->imprime_topo('home');?>
    <?php //$PAGE_BANNERS->mostra_banners();?> 
    <section id="conteudos">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            	<h1 style="color:#000;">LOCALIZA&Ccedil;&Atilde;O</h1>
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <div style="overflow:hidden;height:500px;width:100%;"><div id="gmap_canvas" style="height:500px;width:100%;"></div>
				<style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                <script type="text/javascript"> 
					function init_map(){
						var myOptions = {
							zoom:15,
							center:new google.maps.LatLng(-26.946487, -49.268139),
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						
						map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
						marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(-26.946487, -49.268139)});
						infowindow = new google.maps.InfoWindow({content:"<b>Brune`s Ind�stria de Carnes e Defumados</b><br/>Rua Maria Valcanaia, 280 - Estrada das Areias<br/> Indaial - SC " });
						google.maps.event.addListener(marker, "click", function(){
							infowindow.open(map,marker);
						});
						infowindow.open(map,marker);
					}
					google.maps.event.addDomListener(window, 'load', init_map);
                </script>
            </div>	 
		</div>            
	</section> 
    
    <?php echo $PAGE->rodape_site();?>
</div>

<?php
//$PAGE->imprime_topo('home');
/*
?>
  <div id="banners"><?=$PAGE_BANNERS->mostra_banners();?></div>
  <div class="clear"></div>
  <div style="height:450px;"></div>
<? $PAGE->imprime_rodape(true); */?>