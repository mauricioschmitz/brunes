<?php
ini_set('display_errors', 'Off');
error_reporting(false);
// informa��es locais
setlocale(LC_CTYPE,'pt_BR');

// iniciar uma sess�o
session_start();

// constantes servidor
define("URLSITE",    "http://www.frigorificobrunes.com.br");
define("URLADMIN",   URLSITE."/admin");
define("BDIR",      "/home/u861808262/public_html/site");

// constantes localhost
//define("URLSITE",    "http://localhost/sites/brunes");
//define("URLADMIN",   URLSITE."/admin");
//define("BDIR",      "/Users/mauricioschmitz/Sites/brunes");

define("EMAIL",      "brunes.indaial@gmail.com");//
define("TITULO",      "Brunes Ind&uacute;stria de Carnes e Defumados");
define("VERSAO",      "1.0");

header("Content-type: text/html; charset=utf-8");
// seta em que diret�rios o php ir� procurar um arquivo
set_include_path(BDIR."/include_base".';'.BDIR."/include_share");?>