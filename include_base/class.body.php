<?php
class body extends db{
    function __contruct(){
        db :: __construct();
    }
	
//------------------------------------------------------------------------------------------------
    function imprime_cabecalho($parametros, $local){
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

        <html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <link rel="shortcut icon" type="image/ico" href="<?=URLSITE;?>/imagens/logo_icon.png" />
          
          <title><?=$parametros['title'];?></title>
          <meta name="viewport" content="width=device-width, initial-scale=1">
		  <meta content="<?=$parametros['keywords'];?>" name="keywords"/>
  		  
  		  <meta content="<?=$parametros['description'];?>" name="description"/>
		  
          <link rel="stylesheet" type="text/css" href="<?=$local;?>/css/styles.css" />
          
		  <script language="javascript" src="<?=$local;?>/js/ajax.js"></script>
	      <script language="javascript" src="<?=$local;?>/js/funcoes.js"></script>
          <script src="<?=$local;?>/js/jquery-1.4.4.js" language="javascript"></script> 
          <script language="javascript" src="<?=$local;?>/js/script.js"></script>
          <script language="javascript" src="<?=$local;?>/js/superfish.js"></script>
          
          <link rel="stylesheet" type="text/css" href="<?=URLSITE;?>/highslide/highslide.css" />
          <script type="text/javascript" src="<?=URLSITE;?>/highslide/highslide-with-gallery.js"></script>
          <script type="text/javascript" src="<?=URLSITE;?>/highslide/highslide-with-html.js"></script>
          <!--[if lt IE 7]>
          <link rel="stylesheet" type="text/css" href="<?=URLSITE;?>/highslide/highslide-ie6.css" />
          <![endif]-->
          <!--CKEDITOR-->
          <script src="<?=URLSITE;?>/ckeditor/ckeditor.js" type="text/javascript"></script>
          <script type="text/javascript">
            hsh.graphicsDir = 'highslide/graphics/';
    		hsh.outlineType = 'rounded-white';
			
			hs.graphicsDir = 'highslide/graphics/';
            hs.align = 'center';
            hs.transitions = ['expand', 'crossfade'];
            hs.fadeInOut = true;
            hs.dimmingOpacity = 0.8;
            hs.outlineType = 'rounded-white';
            hs.captionEval = 'this.thumb.alt';
            hs.marginBottom = 105; // make room for the thumbstrip and the controls
            hs.numberPosition = 'caption';
            
            // Add the slideshow providing the controlbar and the thumbstrip
            hs.addSlideshow({
              //slideshowGroup: 'group1',
              interval: 5000,
              repeat: false,
              useControls: true,
              overlayOptions: {
                className: 'text-controls',
                position: 'bottom center',
                relativeTo: 'viewport',
                offsetY: -60
              },
              thumbstrip: {
                position: 'bottom center',
                mode: 'horizontal',
                relativeTo: 'viewport'
              }
            });
          </script>
        </head>

        <?php
    }
	
//------------------------------------------------------------------------------------------------
     function imprime_cabecalho_site($parametros, $local){
        ?>
         <!DOCTYPE html>

        <html lang="pt-bt">
        <head>
          <meta charset="utf-8" />
          
          <title><?=$parametros['title'];?></title>
          <meta name="viewport" content="width=device-width, initial-scale=1">
		  <meta content="<?=$parametros['keywords'];?>" name="keywords"/>
  		  
  		  <meta content="<?=$parametros['description'];?>" name="description"/>
          
          <meta name="author" content="Mauricio Schmitz" /> 
          
          <meta name="classification" content="Frigorifico" />
          
          <!-- JQuery JavaScript -->
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
          
          <!-- Bootstrap minified CSS -->
		  <link rel="stylesheet" href="<?=URLSITE;?>/bootstrap-3.3.6-dist/css/bootstrap.min.css">

		  <!-- Bootstrap Optional theme -->
		  <link rel="stylesheet" href="<?=URLSITE;?>/bootstrap-3.3.6-dist/css/bootstrap-theme.min.css" >

		  <!-- Bootstrap  minified JavaScript -->
		  <script src="<?=URLSITE;?>/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>	

		  <!-- Flexslide -->
		  <script src="<?=URLSITE;?>/js/jquery.flexslider.js"></script>	
          <link rel="stylesheet" href="<?=URLSITE;?>/css/flexslider.css" >
		  <!-- FONTAWESOME-->	  
          <link rel="stylesheet" href="font-awesome-4.5.0/css/font-awesome.min.css">

		  <!-- Brunes CSS -->
		  <link rel="stylesheet" href="<?=URLSITE;?>/css/estilo.css">
		  
          
          <!-- Flexslide -->
		  <script src="<?=URLSITE;?>/js/custom.js"></script>
          <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
          <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
          <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
          
          <!--HIGHSLIDE-->
          <link rel="stylesheet" type="text/css" href="<?=$local;?>/highslide/highslide.css" />
          <script type="text/javascript" src="<?=$local;?>/highslide/highslide-with-gallery.js"></script>
          <script type="text/javascript" src="<?=$local;?>/highslide/highslide-with-html.js"></script>
          <!--[if lt IE 7]>
          <link rel="stylesheet" type="text/css" href="<?=$local;?>/highslide/highslide-ie6.css" />
          <![endif]-->
          
          <script type="text/javascript">
            hsh.graphicsDir = 'highslide/graphics/';
    		hsh.outlineType = 'rounded-white';
			
			hs.graphicsDir = 'highslide/graphics/';
            hs.align = 'center';
            hs.transitions = ['expand', 'crossfade'];
            hs.fadeInOut = true;
            hs.dimmingOpacity = 0.8;
            hs.outlineType = 'rounded-white';
            hs.captionEval = 'this.thumb.alt';
            hs.marginBottom = 105; // make room for the thumbstrip and the controls
            hs.numberPosition = 'caption';
            
            // Add the slideshow providing the controlbar and the thumbstrip
            hs.addSlideshow({
              //slideshowGroup: 'group1',
              interval: 5000,
              repeat: false,
              useControls: true,
              overlayOptions: {
                className: 'text-controls',
                position: 'bottom center',
                relativeTo: 'viewport',
                offsetY: -60
              },
              thumbstrip: {
                position: 'bottom center',
                mode: 'horizontal',
                relativeTo: 'viewport'
              }
            });
          </script>
        </head>
       	
<?   
		define("URLATUAL",   $local);
	}


//----------------------------------------------------------------------------------------
    function imprime_topo($menu=false){	
		${$menu} = 'active';
?>	
	<header>
        <div class="row visible-md visible-lg">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1 faixa">
                <div class="row">
                    <div class="col-md-1 text-left cantos-faixa"><img src="<?=URLSITE;?>/imagens/faixa-left.png" /></div>
                    <div class="col-md-10 text-center"><a href="home.php"><img src="<?=URLSITE;?>/imagens/logo.png" /></a></div>
                    <div class="col-md-1 text-right cantos-faixa"><img src="<?=URLSITE;?>/imagens/faixa-right.png" /></div>
                </div>  
                
            </div>
        </div>
        <div class="row hidden-md hidden-lg logo-mobile">
            <div class="col-sm-12 col-xs-12">
                <div class="col-md-10 text-center"><a href="home.php"><img src="<?=URLSITE;?>/imagens/logo.png" /></a></div>
            </div>
        </div>
        <div class="row menu-brunes">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                <!-- Static navbar -->
                <nav class="navbar navbar-default brunes">
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                      </div>
                      <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                          <li class="<?=$empresa;?>"><a href="empresa.php">Empresa</a></li>
                          <li class="dropdown <?=$produtos;?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produtos <span class="caret"></span></a>
                            <ul class="dropdown-menu">
<?php
	$sql = mysql_query("SELECT * FROM categorias WHERE ativo = 'sim' ORDER BY titulo");
	while($reg = mysql_fetch_array($sql)){
		echo '<li><a href="produtos.php?c='.$reg['codigo'].'">'.$reg['titulo'].'</a></li>';
	}
                              
?>
                            </ul>
                          </li>
                          <li class="<?=$noticias;?>"><a href="noticias.php">Notícias</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                          <li class="dropdown <?=$produtos;?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Receitas <span class="caret"></span></a>
                            <ul class="dropdown-menu">
<?php
	$sql = mysql_query("SELECT * FROM categorias_receitas ORDER BY titulo");
	while($reg = mysql_fetch_array($sql)){
		echo '<li><a href="receitas.php?c='.$reg['codigo'].'">'.$reg['titulo'].'</a></li>';
	}
                              
?>
                            </ul>
                          </li>
                          <li class="<?=$onde_encontrar;?>"><a href="onde-encontrar.php">Onde Encontrar</a></li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contato <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="contato.php">Fale Conosco</a></li>
                              <li><a href="localizacao.php">Localização</a></li>
                            </ul>
                          </li>
                        </ul>
                      </div><!--/.nav-collapse -->
                </nav>
            </div>
        </div> 
    </header>    
<?php
	}
	
//----------------------------------------------------------------------------------------
    function menu($menu=false){		
		if($menu =='home')
			$home = 'class="current"';
			
		else if($menu =='alt_tex')
			$textos = 'class="current"';
			
		else if($menu =='cad_not')
			$noticias = 'class="current"';
			
		else if($menu =='cad_cat')
			$produtos = 'class="current"';
			
		else if($menu =='cad_pro')
			$produtos = 'class="current"';
			
		else if($menu =='cad_cli')
			$clientes = 'class="current"';
			
		else if($menu =='cad_rec')
			$receitas = 'class="current"';
			
		else if($menu =='cad_cat_rec')
			$receitas = 'class="current"';
			
		else if($menu =='cad_ban')
			$banners = 'class="current"';
			
		echo '
            <ul class="sf-menu">
              <li '.$home.'><a href="admin.php">Home</a></li>';
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$textos.'><a href="javascript:void();">Textos</a>
				<ul>
                  <li><a href="admin.php?menu=alt_tex&tela=empresa">Empresa</a></li>
				  <li><a href="admin.php?menu=alt_tex&tela=bemvindo">Bem Vindo</a></li>
				  <li><a href="admin.php?menu=alt_tex&tela=noticias">Chamada Notícias</a></li>
				  <li><a href="admin.php?menu=alt_tex&tela=onde_encontrar">Onde encontrar</a></li>
                </ul>
			  </li>';
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$produtos.'><a href="javascript:void(0)">Produtos</a>
				<ul>
                  <li><a href="admin.php?menu=cad_cat">Categorias</a></li>
                  <li><a href="admin.php?menu=cad_pro">Produtos</a></li>
                </ul>
			  </li>';
		
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$clientes.'><a href="admin.php?menu=cad_cli">Clientes</a></li>';
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$novidades.'><a href="admin.php?menu=cad_not">Notícias</a></li>';
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$receitas.'><a href="javascript:void(0)">Receitas</a>
				<ul>
                  <li><a href="admin.php?menu=cad_cat_rec">Categorias</a></li>
                  <li><a href="admin.php?menu=cad_rec">Receitas</a></li>
                </ul>
			  </li>';
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$banners.'><a href="admin.php?menu=cad_ban">Banners</a></li>';
		
		if($this->testa_modulos('ADMIN')) echo'
			  <li '.$configuracoes.'><a href="admin.php?menu=cad_config">Configurações</a></li>';

		echo '
			</ul>  
            <div class="clear"></div>';
	}

//----------------------------------------------------------------------------------------
    function testa_modulos($nivel){	
		$niveis = explode("|",$nivel);
		
		if($_SESSION['NIVEL_USUARIO'] == 'ADMIN') $tabela = 'usuarios';
		
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $tabela WHERE codigo = ".$this->anti_sql_injection($_SESSION['USUARIO_ID'])));
		if(in_array($_SESSION['NIVEL_USUARIO'], $niveis) && $reg['codigo']){
			return true;
		}else{
			return false;	
		}
	}

//----------------------------------------------------------------------------------------
    function rodape_site(){	
?>	
	<footer>
    	<div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="row">
                    <div class="col-md-6">
                        <div class="widget text-center">
                            <h5 class="h3">Nossos contatos</h5>
                            <address>
                            <strong>Brune's Indústria de Carnes e Defumados</strong><br>
                            Rua Maria Valcanaia, 280 - Estrada das Areias<br>
                            Indaial - SC - 89081-415
                             </address>
                            <p>
                                <i class="fa fa-phone"></i> (47) 3394-8804 <br>
                                <i class="fa fa-envelope"></i> brunes.indaial@gmail.com
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="widget text-center">
                            <h5 class="h3">Links</h5>
                            <ul class="link-list">
                                <li><a href="noticias.php">Últimas Notícias</a></li>
                                <li><a href="receitas.php">Receitas</a></li>
                                <li><a href="produtos.php">Produtos</a></li>
                                <li><a href="onde-encontrar.php">Onde Encontrar</a></li>
                                <li><a href="localizacao.php">Localização</a></li>
                                <li><a href="contato.php">Contato</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div id="sub-footer">  
        	<div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <p>
                            <span>&copy; Brune's Indústria de Carnes e Defumados 2015 Todos direitos reservados. By </span><a href="http://www.mauricioschmitz.com.br" target="_blank">Mauricio Schmitz</a>
                        </p>
                    </div>
                </div>
                <div style="clear:both; margin-bottom:30px;"></div>
            </div> 
        </div>        
    </footer>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-80600319-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
<?php
	}
//----------------------------------------------------------------------------------------
    function rodape(){	
		echo '
		<div style="width:100%; border-top:5px solid #6bbdfa; margin: 10px 0; ">
		  <div style="float:left; padding:10px; font-weight:bold; color:#6bbdfa;">'.TITULO.' | 2014 - Todos os direitos reservados</div>
		  <div style="float:right; padding:10px; font-weight:bold; color:#6bbdfa;">Desenvolvido por: <a href="http://www.mauricioschmitz.com.br">Mauricio Schmitz</a></div>
		</div>
		';
	}
	

//-----------------------------------------------------------------------------------------------	
	function dispara_email($fromName,$rmail,$rnome,$Address,$subject,$body,$cabecalho=false, $admin=false) {    		
		$mailer = new PHPMailer();
		//$mailer->IsSMTP();
		$mailer->ISHTML(true);
		$mailer->SMTPDebug = 0;
		$mailer->Port = 587; //Indica a porta de conexão para a saída de e-mails
		//$mailer->SMTPSecure = "tls";
		$mailer->Host = 'mx1.weblink.com.br';
		$mailer->SMTPAuth = false; //define se haverá ou não autenticação no SMTP
		$mailer->Username = 'noreply@frigorificobrunes.com.br'; //Informe o e-mai o completo
		$mailer->Password = 'w8BYtMwpZ4Cm'; //Senha da caixa postal
		
		$mailer->FromName = $fromName; //Nome que será exibido para o destinatário
		$mailer->From = EMAIL;//'noreply@frigorificobrunes.com.br';//EMAIL; //Obrigatório ser a mesma caixa postal indicada em "username"
		$mailer->AddReplyTo($rmail , $rnome);
		
		//Adiciona os email de detinatário
		$mails = explode(",",$Address);
		for($i=0;$i<count($mails);$i++){
			$x = explode("|",$mails[$i]);
			$email = $x[0];
			if($x[1])
				$nome = $x[1];
			else
				$nome = $email;
				
			$mailer->AddAddress($email, $nome); //Destinatários		
		}
		
		$mailer->Subject = $subject;
		
		//Corpo do email!
		if($cabecalho){
			$header = '
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Mauricio Schmitz</title>
  <style type="text/css">
  body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
  }
  body {
	margin-left: 0px;
	margin-top: 10px;
	margin-right: 0px;
	margin-bottom: 0px;
  }
  a{
	color: #000000;
	text-decoration: none;
  }
  .signinme{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000066;
  }
  .cinza{
	color: #666666;
  }
  </style>
</head>

<body>
<table width="100%" border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">';
	
			$footer = '
	</td>
  </tr>
  <tr>
    <td style="border-bottom: #CCCCCC 2px solid;">&nbsp;</td>
  </tr>
  <tr>
    <td>
      <p style="float: left; margin: 0; padding: 0; margin-top: 3px;" class="signinme">
        Musical JP<br />
        E-mail: <a href="mailto:'.EMAIL.'" class="cinza">'.EMAIL.'</a><br />
		<a href="'.URLSITE.'" class="cinza">'.URLSITE.'</a>
      </p>
    </td>
  </tr>
</table>
</body>
</html>			';

			
		}
		$mailer->Body = $header.$body.$footer;
		
		if($mailer->Send())
			return true;
		else
			return false;
				
	}
		
	
//----------------------------------------------------------------------------------------
    function imprime_rodape($site=false,$menu=false){
		if($site){
?>					
		 <div id="rodape">
           <p class="f14 destaque">Brune’s Indústria de Carnes e Defumados | 2014 - Todos os direitos reservados</p>
           <br>
           <p>Rua Maria Valcanaia , 280  Estrada das Areias Indaial SC.</p>
           <p>Fone : 047 3394-8804</p>
           <p>Desenvolvido por: <a href="http://www.mauricioschmitz.com.br" target="_blank" class="branco">Mauricio Schmitz</a></p>
         </div>
<?
		}
?>          
        </body>
        </html>
<?php
    }
	
//----------------------------------------------------------------------------------------
	function anti_sql_injection($str) {
	    if (!is_numeric($str)) {
	        $str = get_magic_quotes_gpc() ? stripslashes($str) : $str;
	        $str = function_exists('mysql_real_escape_string') ? mysql_real_escape_string($str) : mysql_escape_string($str);
	    }
	    return $str;
	}	
		
	
//----------------------------------------------------------------------------------------
	function verifica_acesso(){
		if($_SESSION['NIVEL_USUARIO'] == 'ADMIN'){
			$reg = mysql_fetch_array(mysql_query("SELECT * FROM usuarios WHERE codigo = '".$_SESSION['USUARIO_ID']."'"));
			if(!$reg['codigo']){
				header('location: '.URLADMIN);
				exit();
			}
		}else{
			header('location: '.URLADMIN);
			exit();
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars(trim($str)));
		
	}
	
//------------------------------------------------------------------------------------------------	
	function verifica_username(){
		if(!isset($_SESSION['USERNAME'])){
			echo '
			  <script language="javascript">
				location.href = "entrar.php";
			  </script>
			';
			
			exit;
		}
	}
	
//------------------------------------------------------------------------------------------------
	function formata_data($data, $divisor, $marcador){
		if($data){
			$array = explode($divisor, $data);
			
			return $array[2].$marcador.$array[1].$marcador.$array[0];
		}
	}
	
	
//------------------------------------------------------------------------------------------------
	function mostra_data(){
		$dia_da_semana = array("Domingo", "Segunda - feira", "Ter&ccedil;ça - feira","Quarta - feira","Quinta - feira","Sexta - feira","S&aacute;bado");
		$meses = array("Janeiro","Fevereiro","Mar&ccedil;o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
		$num_dia = date(w);
		$num_mes = date(m);
		$dia_extenso = $dia_da_semana[$num_dia];
		echo $dia_extenso.", " .date("d")." de ".$meses[$num_mes-1]." de ".date("Y");
	}	
//----------------------------------------------------------------------------------------
	function seleciona($valor1,$valor2,$retorno) {
	    if ($valor1 == $valor2) {
	        return $retorno;
	    }
	}
	
//----------------------------------------------------------------------------------------
	function imagem($img,$t) {
		return str_replace(".jpg",$t.".jpg",str_replace(".jepg",$t.".jepg",str_replace(".png",$t.".png",$img)));
	}

//----------------------------------------------------------------------------------------
	function mes_extenso($mes) {
	    $array = array('','Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez');
		
		return $array[$mes];
	}	
//----------------------------------------------------------------------------------------

	function limita_str($str, $limit){
		if (strlen($str)>$limit){
			$str = substr($str,0,$limit);
			$ultChr = strrpos($str,' ');
			$str = substr($str,0,$ultChr).'...';
		}
		return $str;
	}				
}
?>