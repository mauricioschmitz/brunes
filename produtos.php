<?php
require 'path.php';

require 'include_share/class.produtos.php';

$PAGE = new body();
$PAGE_PRODUTOS = new produtos();

$PAGE->conecta_banco();

//parametros para otimização do google
$parametros['title'] = TITULO;
$parametros['keywords'] = "";
$parametros['description'] = "";
$PAGE->imprime_cabecalho_site($parametros, URLSITE);
?>

<div class="container-fluid">  
    <?=$PAGE->imprime_topo('home');?>
    <?php //$PAGE_BANNERS->mostra_banners();?> 
    <section id="conteudos">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            	<h1 style="color:#000;">PRODUTOS</h1>
                <?php
				if(isset($_GET['id']) && is_numeric($_GET['id'])){
					$PAGE_PRODUTOS->detalhes_produto($_GET['id']);
				}else{
					$PAGE_PRODUTOS->mostra_produtos($_GET['c']);
				}
				?>
            </div>	 
		</div>            
	</section> 
    
    <?php echo $PAGE->rodape_site();?>
</div>

<?php
//$PAGE->imprime_topo('home');
/*
?>
  <div id="banners"><?=$PAGE_BANNERS->mostra_banners();?></div>
  <div class="clear"></div>
  <div style="height:450px;"></div>
<? $PAGE->imprime_rodape(true); */?>