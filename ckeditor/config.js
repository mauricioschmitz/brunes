﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	
	CKEDITOR.config.toolbar = 
            [
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo','Redo'],
                ['Find','Replace','-','SelectAll','RemoveFormat'],
                ['Link', 'Unlink', 'Image', 'Table','TableTools', 'HorizontalRule'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'], 
				['Bold', 'Italic', 'Underline'],
                ['Font', 'FontSize'],
                ['TextColor'],
                ['NumberedList','BulletedList','-','Blockquote'],
                ['PageBreak','Maximize']
            ]
	config.htmlEncodeOutput = false;
	config.entities = false;		
	// Define changes to default configuration here. For example:
	config.language = 'pt-BR';
	// config.uiColor = '#AADC6E';
};
