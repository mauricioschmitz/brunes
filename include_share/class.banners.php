<?php
class banners extends body{
    
    function __construct(){
        body :: __construct();
		banners :: tabela_banners();
    }

//------------------------------------------------------------------------------------------------
	function tabela_banners(){
		$this->tabela_banners          = 'banners';
		$this->banners_codigo          = 'codigo';
		$this->banners_data_cadastro   = 'data_cadastro';
		$this->banners_titulo          = 'titulo';
		$this->banners_link            = 'link';
		$this->banners_imagem          = 'imagem';
		$this->banners_width           = '1440';
		$this->banners_height          = '520';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo          = $this->banners_codigo;
		$data_cadastro   = $this->banners_data_cadastro;
		$titulo          = $this->banners_titulo;
		$link            = $this->banners_link;
		$imagem          = $this->banners_imagem;

		if($upd == true){
			$nome_tela   = 'Alterar banner';
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_ban&atualizar=true&codigo='.$_GET['codigo'];
		}else{
			$nome_tela   = 'Novo banner';
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_ban&inserir=true';
		}
		
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 550px; text-align: left; margin: auto;">';
		
		if($upd)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
            <tr>
              <td><label for="<?=$titulo;?>">T&iacute;tulo *</label></td>
			  <td><input type="text" name="<?=$titulo;?>" id="t�tulo" value="<?=$post[$titulo];?>" maxlength="100" style="width: 300px;" class="toolTip2" tooltip2="Informe um titulo" /></td>
		    </tr>
            <tr>
              <td><label for="<?=$link;?>">Link </label></td>
			  <td><input type="text" name="<?=$link;?>" value="<?=$post[$link];?>" maxlength="200" style="width: 300px;" class="toolTip2" tooltip2="Informe um link, iniciar com http://" /></td>
		    </tr>
<?php
		if($upd != false){
			echo '
			<tr>
			  <td></td>
			  <td>';
			if(!$post[$imagem]){
			    echo "<span style=\"color: #ff0000;\">Nenhum arquivo cadastrado.</span>";
			}else{
				$ext = explode(".",$post[$imagem]);
				$qt = count($ext);
				$ext = ".".$ext[$qt-1];
				$arquivo = current(explode(".",$post[$imagem]));
				$arquivo = strtr($arquivo, "_", " ");
				
				echo '
				<div style="margin: 2px 0 2px 0;">
				  <a href="arquivos/banners/'.$post[$imagem].'" rel="highslide" onclick="return hs.expand(this);">'.$arquivo.$ext.'</a>
				</div>';
			}
			echo '
			  </td>
			</tr>';
		}else{
			$id_campo = 'id="imagem"';
			$obrig = '*';
		}
?>
			<tr>
			  <td valign="top" style="padding-top: 5px;"><label for="<?=$imagem;?>">imagem <?=$obrig;?></label></td>
			  <td>
                <input type="file" name="<?=$imagem;?>[]" <?=$id_campo;?> size="30" style="width: 300px;" /><br />
                Largura <?=$this->banners_width;?>px Altura <?=$this->banners_height;?>px.<br />
              </td>
			</tr>
		  </table>
		  <br />
          <p>* Campos destacados s&atilde;o obrigat&oacute;rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_ban" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" class="bts"  id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" onclick="envia_banner(this.form)" />
	      </center>
		</form>
        <?php
    }
	
//------------------------------------------------------------------------------------------------
    function lista_banners($erro=false){
        $sql = mysql_query("SELECT * FROM $this->tabela_banners ORDER BY $this->banners_data_cadastro");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de banners</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_ban&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Novo</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
        	      <td class="lista_tit cel_tabela" width="12%" align="left">Data</td>
        	      <td class="lista_tit cel_tabela" width="42%" align="left">T&iacute;tulo</td>
				  <td class="lista_tit cel_tabela" width="30%" align="left">Arquivo</td>
        	      <td class="lista_tit cel_tabela" colspan="2" width="16%" align="center">
				    <p style="width: 100px;">A&ccedil;&otilde;es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			$array = explode("-",$reg[$this->banners_data_cadastro]);
			$reg[$this->banners_data_cadastro] = $array[2]."/".$array[1]."/".$array[0];
			
			$ext = explode(".",$reg[$this->banners_imagem]);
			$qt = count($ext);
			$ext = ".".$ext[$qt-1];
			$arquivo = current(explode(".",$reg[$this->banners_imagem]));
			$arquivo = strtr($arquivo, "_", " ");
				
            echo '
			    <tr>
                  <td class="cel_tabela" align="left">'.$reg[$this->banners_data_cadastro].'</td>
                  <td class="cel_tabela" align="left">'.$reg[$this->banners_titulo].'</td>
				  <td class="cel_tabela" align="left"><a href="'.URLADMIN.'/arquivos/banners/'.$reg[$this->banners_imagem].'" rel="highslide" onclick="return hs.expand(this)">'.$arquivo.$ext.'</a></td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_ban&acao=edit&codigo='.$reg[$this->banners_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_ban&acao=excl&codigo='.$reg[$this->banners_codigo].'&arquivo='.$reg[$this->banners_imagem].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_banner($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_banners WHERE $this->banners_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_banner($codigo, $arquivo){
		@unlink('arquivos/banners/'.trim($arquivo));
		
		mysql_query("DELETE FROM $this->tabela_banners WHERE $this->banners_codigo = ".$codigo);
		
		$this->lista_banners();
	}

//------------------------------------------------------------------------------------------------
	function insere_banner($post){
		require "upload/upload.php";
		
		$imagem = uploadFotos("1500", $_FILES[$this->banners_imagem], "arquivos/banners/", false);
		
		if(!$imagem || $imagem == "invalido"){
			$this->imprime_form($post, false, "Arquivo em formato inv�lido. Os Arquivos devem ser no formato JPG ou JPEG.");
			
		}else{
			mysql_query("
			  INSERT INTO $this->tabela_banners (
				  $this->banners_data_cadastro
				, $this->banners_titulo
				, $this->banners_link
				, $this->banners_imagem
			  )VALUES (
				  NOW()
				, '".$this->LimpaString($post[$this->banners_titulo])."'
				, '".$this->LimpaString($post[$this->banners_link])."'
				, '".$imagem."'
			  )");
			$codigo = mysql_insert_id();
			if(mysql_error()){
				@unlink('arquivos/banners/'.trim($imagem));
				$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.");
				
            }else{
				$this->recorta_imagem($codigo);
				
			}
		}
    }
	
//------------------------------------------------------------------------------------------------
	function getWidthHeight($image,$d) {
		$size = getimagesize($image);
		$height = $size[1];
		$width = $size[0];
		if($d == 'h')
			return $height;
		else
			return $width;	
	}
	
//------------------------------------------------------------------------------------------------
	function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
		}
		imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$thumb_image_name); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$thumb_image_name,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);  
				break;
		}
		chmod($thumb_image_name, 0777);
		return $thumb_image_name;
	}	
//------------------------------------------------------------------------------------------------
	function recorta_imagem($codigo,$recortar=false){	
		//Define o tamanho do banner
		$thumb_width = $this->banners_width;
		$thumb_height = $this->banners_height ;
		
		//Pega o nome da imagem
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_banners WHERE codigo=".$codigo));
		$imagem = $reg['imagem'];
		
		$current_large_image_width =  $this->getWidthHeight('arquivos/banners/'.$reg['imagem'],'w');
		$current_large_image_height = $this->getWidthHeight('arquivos/banners/'.$reg['imagem'],'h');
		
		if($recortar){
			$x1 = $_POST["x1"];
			$y1 = $_POST["y1"];
			$x2 = $_POST["x2"];
			$y2 = $_POST["y2"];
			$w = $_POST["w"];
			$h = $_POST["h"];
			//Scale the image to the thumb_width set above
			$scale = $thumb_width/$w;
			
			
			$cropped = $this->resizeThumbnailImage('arquivos/banners/'.$reg['imagem'], 'arquivos/banners/'.$reg['imagem'],$w,$h,$x1,$y1,$scale);
			$this->lista_banners();
		}else{
		
?>		
	<script type="text/javascript" src="js/jquery.imgareaselect.min.js"></script>
	<script type="text/javascript">
		function preview(img, selection) { 
			var scaleX = <?php echo $thumb_width;?> / selection.width; 
			var scaleY = <?php echo $thumb_height;?> / selection.height; 
			
			$('#thumbnail + div > img').css({ 
				width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
				height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
				marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
				marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
			});
			$('#x1').val(selection.x1);
			$('#y1').val(selection.y1);
			$('#x2').val(selection.x2);
			$('#y2').val(selection.y2);
			$('#w').val(selection.width);
			$('#h').val(selection.height);
		} 
		
		$(document).ready(function () { 
			$('#save_thumb').click(function() {
				var x1 = $('#x1').val();
				var y1 = $('#y1').val();
				var x2 = $('#x2').val();
				var y2 = $('#y2').val();
				var w = $('#w').val();
				var h = $('#h').val();
				if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
					alert("You must make a selection first");
					return false;
				}else{
					return true;
				}
			});
		}); 
		
		$(window).load(function () { 
			$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?php echo $thumb_height/$thumb_width;?>', onSelectChange: preview }); 
		});
		
		$(document).ready(function () {
		  $('#thumbnail').imgAreaSelect({ x1: 0, y1: 0, x2: <?=$this->banners_width;?> , y2: <?=$this->banners_height;?> });
		});
	</script>
    <table width="<?=$current_large_image_width;?>" style="margin:auto;">
      <tr>
        <td>
          <h2>Recortar a imagem</h2>
            Original:<br />
            <img src="<?=URLADMIN.'/arquivos/banners/'.$reg['imagem'];?>" style="float: left; margin-right: 10px;" id="thumbnail" alt="Create Thumbnail" /><br />
            <br style="clear:both;"/>
            <form name="thumbnail" action="admin.php?menu=cad_ban&acao=recorta_imagem&codigo=<?=$codigo;?>" method="post">
                <input type="hidden" name="x1" value="0" id="x1" />
                <input type="hidden" name="y1" value="0" id="y1" />
                <input type="hidden" name="x2" value="<?=$this->banners_width;?>" id="x2" />
                <input type="hidden" name="y2" value="<?=$this->banners_height;?>" id="y2" />
                <input type="hidden" name="w" value="<?=$this->banners_width;?>" id="w" />
                <input type="hidden" name="h" value="<?=$this->banners_height;?>" id="h" />
                <input type="hidden" name="img" value="<?=$arquivo;?>" id="h" />
                <input type="submit" name="upload_thumbnail" value="Salvar" class="bts" id="save_thumb" />
            </form>
          </div>
        </td>
      </tr>
    </table>    
      
<?  
		}  
		
	}
//------------------------------------------------------------------------------------------------
	function atualiza_banner($post){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_banners WHERE $this->banners_codigo = ".$post[$this->banners_codigo]));
		
		require "upload/upload.php";
		
		$imagem = uploadFotos("1500", $_FILES[$this->banners_imagem], "arquivos/banners/", false);
		
		if($imagem == "invalido"){
			$post[$this->banners_imagem] = $reg[$this->banners_imagem];
			$this->imprime_form($post, false, "Arquivo em formato inv�lido. Os Arquivos devem ser no formato JPG ou JPEG.");
			
		}else{
			if($imagem) $coluna = ", $this->banners_imagem = '".$imagem."'";
			
			mysql_query("
			  UPDATE $this->tabela_banners SET
				  $this->banners_titulo   = '".$this->LimpaString($post[$this->banners_titulo])."'
				, $this->banners_link     = '".$this->LimpaString($post[$this->banners_link])."'
				  $coluna
			  WHERE $this->banners_codigo = ".$post[$this->banners_codigo]);
			
			if(mysql_error()){
				@unlink('arquivos/banners/'.trim($imagem));
				$post[$this->banners_imagem] = $reg[$this->banners_imagem];
				$this->imprime_form($post, true, "Ocorreu um erro ao inserir o registro. Por favor verifique.");
				
            }else{
				if($imagem){
					 @unlink('arquivos/banners/'.$reg[$this->banners_imagem]);
					 $this->recorta_imagem($post[$this->banners_codigo]);
				}else{ 
					$this->lista_banners();
				}
			}
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}
	
//------------------------------------------------------------------------------------------------	
	function mostra_banners(){
		$sql = mysql_query("SELECT * FROM $this->tabela_banners ORDER BY RAND()");
?>
        <section id="banner">
        <div class="row">	 
            <!-- Slider -->
            <div id="main-slider" class="flexslider">
                <ul class="slides">
<?php
		while($reg = mysql_fetch_array($sql)){
			if($reg[$this->banners_link]){
				$abre = '<a href='.$reg[$this->banners_link].'>';
				$fecha = '</a>';
			}
			
			echo '<li>'.$abre.'<img src="'.URLADMIN.'/arquivos/banners/'.$reg[$this->banners_imagem].'" />'.$fecha.'<!--<div class="flex-caption container">
                        <h3>Loren ipsum</h3> 
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>  
                    </div>--></li>';
			
			$abre = '';
			$fecha = '';
		}
?>
                  
                </ul>
            </div>
            <!-- end slider -->
        </div>
	</section>
    <?php
		
	}
}

?>