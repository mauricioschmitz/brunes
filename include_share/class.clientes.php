<?php
class clientes extends body{
    
    function __construct(){
        body :: __construct();
		clientes :: tabela_clientes();
    }

//------------------------------------------------------------------------------------------------
	function tabela_clientes(){
		$this->tabela_clientes        = 'clientes';
		$this->clientes_codigo        = 'codigo';
		$this->clientes_uf            = 'uf';
		$this->clientes_cod_municipio = 'cod_municipio';
		$this->clientes_titulo        = 'titulo';
		$this->clientes_descricao     = 'descricao';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo         = $this->clientes_codigo;
		$cod_municipio  = $this->clientes_cod_municipio;
		$uf             = $this->clientes_uf;
		$titulo         = $this->clientes_titulo;
		$descricao      = $this->clientes_descricao;
		$imagens        = $this->clientes_imagens;

		if($upd == true){
			$nome_tela   = "Alterar cliente";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_cli&codigo='.$_GET['codigo']."&atualizar=true";
		}else{
			$nome_tela   = "Novo cliente";
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_cli&inserir=true';
		}
		
		if(!$post['uf'])
			$post['uf'] = 'SC';
			
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 500px; text-align: left; margin: auto;">';
		
		if($upd == true)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
		    <tr>
              <td><label for="<?=$uf;?>">Estado *</label></td>
			  <td>
              	<select name="<?=$uf;?>" id="<?=$uf;?>" style="width: 315px;"  class="toolTip2" tooltip2="Informe um estado" onchange="$('#<?=$cod_municipio;?>').load('admin.php?menu=cad_cli&acao=set_uf&uf='+this.value);">
                <?php
				$sql = mysql_query("SELECT uf FROM municipio GROUP BY uf ORDER BY uf");
				while($reg = mysql_fetch_array($sql)){
					echo '<option value="'.$reg['uf'].'" '.($post['uf'] == $reg['uf'] ? 'selected':'').'>'.$reg['uf'].'</option>';
				}
				?>
                </select>
              </td>
		    </tr>
            <tr>
              <td><label for="<?=$cod_municipio;?>">Cidade *</label></td>
			  <td>
              	<select name="<?=$cod_municipio;?>" id="<?=$cod_municipio;?>" style="width: 315px;"  class="toolTip2" tooltip2="Informe um munic�pio">
                <?php
				$sql = mysql_query("SELECT codigo, nome FROM municipio WHERE uf = '".$post['uf']."' ORDER BY nome");
				while($reg = mysql_fetch_array($sql)){
					echo '<option value="'.$reg['codigo'].'" '.($post[$cod_municipio] == $reg['codigo'] ? 'selected':'').'>'.utf8_encode($reg['nome']).'</option>';
				}
				?>
                </select>
              </td>
		    </tr>
            <tr>
              <td><label for="<?=$titulo;?>">Titulo *</label></td>
			  <td><input name="<?=$titulo;?>" type="text" id="<?=$titulo;?>" value="<?=$post[$titulo];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe um t�tulo"/></td>
		    </tr>
		    <tr>
			  <td height="20"><label for="<?=$descricao;?>" style="margin-bottom: 2px;">Descri&ccedil;&atilde;o</label></td>
            </tr>
            <tr>
              <td colspan="2">
                <textarea name="<?=$descricao;?>" style="width: 100%; height: 200px;"><?=$post[$descricao];?></textarea>
                <script type="text/javascript">
				  CKEDITOR.replace( '<?=$descricao;?>',
					{
                       /*toolbar :
						[
							[ 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ]
						],*/
						height : 200,
						disableObjectResizing : true,
						removePlugins : 'smiley',
						filebrowserBrowseUrl : '../ckeditor/filebrowser.php?action=browse',
						filebrowserUploadUrl : '../ckeditor/filebrowser.php?action=upload'
				  } );
				</script>
              </td>
		    </tr>
		  </table>
		  <br />
		  <br />
		  <p>* Campos destacados s&atilde;o obrigat&oacute;rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_cli" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" class="bts" onclick="envia_cliente(this.form)" />
	      </center>
		</form>
        <?php
    }

//------------------------------------------------------------------------------------------------
    function lista_clientes($erro=false){
		$sql = mysql_query("SELECT * FROM $this->tabela_clientes ORDER BY $this->clientes_titulo");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de clientes</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_cli&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Novo</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
        	      <td class="lista_tit cel_tabela" width="84%" align="left">T&iacute;tulo</td>
        	      <td class="lista_tit cel_tabela" colspan="3" width="16%" align="center">
				    <p style="width: 150px;">A&ccedil;&otilde;es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			//formata data
			echo '
			    <tr>
                  <td class="cel_tabela" align="left">'.$reg[$this->clientes_titulo].'</td>
                  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_cli&acao=edit&codigo='.$reg[$this->clientes_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_cli&acao=excl&codigo='.$reg[$this->clientes_codigo].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_cliente($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_clientes WHERE $this->clientes_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_cliente($codigo){
		mysql_query("DELETE FROM $this->tabela_clientes WHERE $this->clientes_codigo = ".$codigo);
		
		$this->lista_clientes();
	}
	

//------------------------------------------------------------------------------------------------
	function insere_cliente($post){
		mysql_query("
		  INSERT INTO $this->tabela_clientes (
			  $this->clientes_titulo
			, $this->clientes_uf
			, $this->clientes_cod_municipio
			, $this->clientes_descricao
		  )VALUES (
			  '".$this->LimpaString($post[$this->clientes_titulo])."'
			, '".$post[$this->clientes_uf]."'
			, '".$post[$this->clientes_cod_municipio]."'
			, '".$post[$this->clientes_descricao]."'
		  )");
		
		if(mysql_error()){
			$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.<br>".mysql_error());
			
		}else{
			$this->lista_clientes();
			
		}
    }

//------------------------------------------------------------------------------------------------
	function atualiza_cliente($post){
		mysql_query("
			  UPDATE $this->tabela_clientes SET
				  $this->clientes_titulo        = '".$this->LimpaString($post[$this->clientes_titulo])."'
				, $this->clientes_descricao     = '".$post[$this->clientes_descricao]."'
				, $this->clientes_uf            = '".$post[$this->clientes_uf]."'
				, $this->clientes_cod_municipio = '".$post[$this->clientes_cod_municipio]."'
			  WHERE $this->clientes_codigo = ".$post[$this->clientes_codigo]);
			
			$this->lista_clientes();
		
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}



//------------------------------------------------------------------------------------------------
	function mostra_clientes(){
        $sql_uf = mysql_query("SELECT uf FROM $this->tabela_clientes GROUP BY $this->clientes_uf ORDER BY $this->clientes_uf ASC");
		
		echo '<p>Selecione a cidade desejada:</p>
			  <select name="uf" onchange="$(\'#cidade\').load(\'onde-encontrar.php?menu=set_uf&uf=\'+this.value);">';
		while($reg_uf = mysql_fetch_array($sql_uf)){
			if(!$uf)
				$uf = $reg_uf['uf'];
			echo '<option value="'.$reg_uf['uf'].'">'.$reg_uf['uf'].'</option>';
		}
		echo '</select>&nbsp;&nbsp;';
		
		$sql_cid = mysql_query("SELECT m.codigo, m.nome FROM $this->tabela_clientes c, municipio m WHERE c.cod_municipio = m.codigo AND m.uf = '".$uf."' ORDER BY m.nome ASC");
		
		echo '<select name="cidade" id="cidade" onchange="$(\'#detalhes_clientes\').load(\'onde-encontrar.php?menu=set_mun&mun=\'+this.value);">
			      <option value="0">Selecione</option>';
		while($reg_cid = mysql_fetch_array($sql_cid)){
			echo '<option value="'.$reg_cid['codigo'].'">'.$reg_cid['nome'].'</option>';
		}
		
		echo '</select>
			  <br><br><br>
			  <p><img src="imagens/mapa1.png" style="width:100%" class="visible-sm visible-md visible-lg"></p>';
		
    }
	
//------------------------------------------------------------------------------------------------	
	function detalhes_cliente($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_clientes WHERE $this->clientes_codigo = ".$this->anti_sql_injection($codigo)));
		
		//formata data
		$array = explode("-", $reg[$this->clientes_data_cliente]);
		$reg[$this->clientes_data_cliente] = $array[2]."/".$array[1]."/".$array[0];
		
		//busca a 1� imagem
		$arquivos = explode(",", $reg[$this->clientes_imagens]);
		
		echo '<p>'.$reg[$this->clientes_data_cliente].'</p>
			  <p>'.$reg[$this->clientes_titulo].'</p>
			  <div class="ck">'.$reg[$this->clientes_descricao].'</div>';
		
		if($reg[$this->clientes_imagens]){
			$contador = 0;
			echo '
			    <div class="row">';
			for($i=0; $i<count($arquivos); $i++){
				if($contador >= 3){
					echo '
			    </div>
			    <div class="row">';
					$contador = 0;
				}
				$contador++;
				
				echo '
				<div class="col-md-4">
                  <div class="post3">
				    <a href="'.URLADMIN.'/arquivos/clientes/'.$arquivos[$i].'" rel="highslide" onclick="return hs.expand(this)">
					  <img src="'.URLADMIN.'/arquivos/clientes/'.str_replace(".jpg","_mini.jpg", $arquivos[$i]).'"  alt=""/>
				    </a>
				  </div>
				</div>';
			}
			echo '
			    </div>';
		}
	}
	
}

?>
