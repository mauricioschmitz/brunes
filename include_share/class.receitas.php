<?php
class receitas extends body{
    
    function __construct(){
        body :: __construct();
		receitas :: tabela_receitas();
    }

//------------------------------------------------------------------------------------------------
	function tabela_receitas(){
		$this->tabela_receitas       = 'receitas';
		$this->receitas_codigo       = 'codigo';
		$this->receitas_cod_categoria= 'cod_categoria';
		$this->receitas_titulo       = 'titulo';
		$this->receitas_descricao    = 'descricao';
		$this->receitas_imagens      = 'imagens';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo       = $this->receitas_codigo;
		$cod_categoria= $this->receitas_cod_categoria;
		$titulo       = $this->receitas_titulo;
		$descricao    = $this->receitas_descricao;
		$imagens      = $this->receitas_imagens;

		if($upd == true){
			$nome_tela   = "Alterar receita";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_rec&codigo='.$_GET['codigo']."&atualizar=true";
		}else{
			$nome_tela   = "Nova receita";
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_rec&inserir=true';
		}
		
		if($post[$data_receita]){
			//formata data
			$array = explode("-", $post[$data_receita]);
			$post[$data_receita] = $array[2]."/".$array[1]."/".$array[0];
		}
		
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 500px; text-align: left; margin: auto;">';
		
		if($upd == true)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
		    <tr>
              <td width="15%"><label for="<?=$cod_categoria;?>">Categoria *</label></td>
			  <td width="85%">
			    <select name="<?=$cod_categoria;?>" id="categoria">
<?php 
		$sql_gru = mysql_query("SELECT * FROM categorias_receitas WHERE ativo ='sim' ORDER BY titulo");
		
		while($reg_gru = mysql_fetch_array($sql_gru)){
			if($post[$cod_categoria] == $reg_gru['codigo']){
				$select = "selected";
			}
			
			echo '<option value="'.$reg_gru['codigo'].'" '.$select.'>'.$reg_gru['titulo'].'</option>';
			$select = "";
		}
?>
			    </select>
		  	  </td>
		    </tr>
            <tr>
              <td><label for="<?=$titulo;?>">Titulo *</label></td>
			  <td><input name="<?=$titulo;?>" type="text" id="<?=$titulo;?>" value="<?=$post[$titulo];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe um t�tulo"/></td>
		    </tr>
		    <tr>
			  <td height="20"><label for="<?=$descricao;?>" style="margin-bottom: 2px;">Descri&ccedil;&atilde;o</label></td>
            </tr>
            <tr>
              <td colspan="2">
                <textarea name="<?=$descricao;?>" style="width: 100%; height: 200px;"><?=$post[$descricao];?></textarea>
                <script type="text/javascript">
				  CKEDITOR.replace( '<?=$descricao;?>',
					{
                       toolbar :
						[
							[ 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', 'Image' , 'Source']
						],
						height : 200,
						disableObjectResizing : true,
						removePlugins : 'smiley',
						filebrowserBrowseUrl : '../ckeditor/filebrowser.php?action=browse',
						filebrowserUploadUrl : '../ckeditor/filebrowser.php?action=upload'
				  } );
				</script>
              </td>
		    </tr>
<?php
		if($upd != false){
			$id = $post[$codigo];
			echo '
			<tr>
			  <td></td>
			  <td id="Imagens'.$id.'">';
			$arquivos = explode("," , $post[$imagens]);
			for($i=0; $i<count($arquivos); $i++){
			    if(!$post[$imagens]){
				    echo "<span style=\"color: #ff0000;\">Nenhuma imagem cadastrada.</span>";
				}else{
				    $ext = ".".end(explode(".",$arquivos[$i]));
					
					$arquivo = current(explode(".",$arquivos[$i]));
					$arquivo = strtr($arquivo, "_", " ");
					echo '
				<div style="margin-top: 3px;">
				  <a href="arquivos/receitas/'.$arquivos[$i].'" rel="highslides" onclick="return hs.expand(this)">'.$arquivo.$ext.'</a> - 
				  <a href="javascript: $(\'#Imagens'.$id.'\').load(\'admin.php?menu=cad_rec&acao=del_item&codigo='.$id.'&item='.$i.'\');" class="menu">Remover</a>
				</div>';
				}
		    }
			echo '
			  </td>
			</tr>';
		}
?>
			<tr>
              <td valign="top" style="padding-top: 8px;"><label for="<?=$imagens;?>">Imagens</label></td>
              <td>
                <input type="file" name="<?=$imagens;?>[]" size="25" style="width: 315px;" />
                
                <p id="Campos"></p>
				
				<a href="javascript: void(0);" onClick="NovoItem('imagens', 'Campos');" class="menu">Anexar outra imagem</a>
              </td>
            </tr>
		  </table>
		  <br />
		  <br />
		  <p>* Campos destacados s&atilde;o obrigat&oacute;rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_rec" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" class="bts" onclick="envia_receita(this.form)" />
	      </center>
		</form>
        <?php
    }

//------------------------------------------------------------------------------------------------
    function lista_receitas($erro=false){
		$sql = mysql_query("SELECT R.*, C.titulo AS categoria FROM $this->tabela_receitas R, categorias_receitas C WHERE R.cod_categoria = C.codigo ORDER BY C.titulo, R.$this->receitas_titulo");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de receitas</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_rec&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Nova</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
				  <td class="lista_tit cel_tabela" width="42%" align="left">Categoria</td>
        	      <td class="lista_tit cel_tabela" width="84%" align="left">T&iacute;tulo</td>
        	      <td class="lista_tit cel_tabela" colspan="3" width="16%" align="center">
				    <p style="width: 150px;">A&ccedil;&otilde;es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			//formata data
			echo '
			    <tr>
                  <td class="cel_tabela" align="left">'.$reg['categoria'].'</td>
                  <td class="cel_tabela" align="left">'.$reg[$this->receitas_titulo].'</td>
                  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_rec&acao=edit&codigo='.$reg[$this->receitas_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_rec&acao=excl&codigo='.$reg[$this->receitas_codigo].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_receita($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_receitas WHERE $this->receitas_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_receita($codigo){
		$arq = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_receitas WHERE $this->receitas_codigo = ".$codigo));
		
		$arquivos = explode("," , $arq[$this->receitas_imagens]);
		
		for($i = 0; $i < count($arquivos); $i++){
			@unlink('arquivos/receitas/'.trim($arquivos[$i]));
			
			$ext = ".".end(explode(".", $arquivos[$i]));
			@unlink('arquivos/receitas/'.str_replace($ext, "_mini".$ext, trim($arquivos[$i])));
		}
		
		mysql_query("DELETE FROM $this->tabela_receitas WHERE $this->receitas_codigo = ".$codigo);
		
		$this->lista_receitas();
	}
	
//------------------------------------------------------------------------------------------------
	function exclui_item($id, $item){
		//consulta arquivos cadastrados
		$Consulta = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_receitas WHERE $this->receitas_codigo = ".$id));
		//gera array dos arquivos cadastrados
		$ArrayArquivos = explode(",", $Consulta[$this->receitas_imagens]);
		//deleta arquivo do disco
		@unlink("arquivos/receitas/".$ArrayArquivos[$item]);
		//Gera nome e remove do disco a img miniatura
		$ext = ".".end(explode(".", $ArrayArquivos[$item]));
		@unlink('arquivos/receitas/'.str_replace($ext, "_mini".$ext, trim($ArrayArquivos[$item])));
		
		//remove item do array
		unset($ArrayArquivos[$item]);
		//gera novo array de elementos
		$count = count($ArrayArquivos);
		for ($i = 0; $i <= $count; $i++) {
			if($ArrayArquivos[$i] != ""){
				//gera novo index pro array
				if(!$NovoIndexArray){ $NovoIndexArray = "1"; }
				if($NovoIndexArray == "1"){
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.$ArrayArquivos[$i];
				} else {
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.",".$ArrayArquivos[$i];
				}
			}
		}
		//imprime itens que nao foram removidos
		$arquivos = explode("," , $Arquivos);
		$NumeroArquivos = count($arquivos);
		if($arquivos[0] != ""){
			for($i=0;$i<$NumeroArquivos;$i++){
				$ext = ".".end(explode(".",$arquivos[$i]));
				$arquivo = current(explode(".",$arquivos[$i]));
				$arquivo = strtr($arquivo, "_", " ");
				echo '<div style="margin-top: 3px;">
						<a href="arquivos/receitas/'.$arquivos[$i].'" target="_blank">'.$arquivo.$ext.'</a> - 
						<a href="javascript: CarregaUrl(\'admin.php?menu=cad_rec&acao=del_item&codigo='.$id.'&item='.$i.'\', \'Imagens'.$id.'\');" class="menu">Remover</a>
					  </div>';
			}
		}else{
			echo '<span style="color: #ff0000;">Nenhuma imagem cadastrada.</span>';
		}
		//grava altera�oes no sql
		mysql_query("UPDATE $this->tabela_receitas SET $this->receitas_imagens='".$Arquivos."' WHERE $this->receitas_codigo = ".$id);
	}

//------------------------------------------------------------------------------------------------
	function insere_receita($post){
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->receitas_imagens], "arquivos/receitas/", true);
		
		if($arquivos == "invalido"){
			$this->imprime_form($post, false, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			exit();
			
		}else{
			mysql_query("
			  INSERT INTO $this->tabela_receitas (
				  $this->receitas_cod_categoria
				, $this->receitas_titulo
				, $this->receitas_descricao
				, $this->receitas_imagens
			  )VALUES (
				  '".$this->LimpaString($post[$this->receitas_cod_categoria])."'
				, '".$this->LimpaString($post[$this->receitas_titulo])."'
				, '".addslashes($post[$this->receitas_descricao])."'
				, '".$arquivos."'
			  )");
			
			if(mysql_error()){
				$array = explode("," , $arquivos);
		        
				for($i=0; $i<count($array); $i++){
					@unlink('arquivos/receitas/'.trim($array[$i]));
					
					$ext = ".".end(explode(".", $array[$i]));
					$arq_mini = str_replace($ext, "_mini".$ext, trim($array[$i]));
					@unlink('arquivos/receitas/'.trim($arq_mini));
				}
				
				$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.<br>".mysql_error());
				exit();
				
            }else{
				$this->lista_receitas();
				
			}
		}
    }

//------------------------------------------------------------------------------------------------
	function atualiza_receita($post){
		//seleciona banco
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_receitas 
		         WHERE codigo = ".$post[$this->receitas_codigo]));
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->receitas_imagens], "arquivos/receitas/", true);
		
		if($arquivos == "invalido"){
			$post[$this->receitas_imagens] = $reg[$this->receitas_imagens];
			$this->imprime_form($post, true, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			
			exit;
		}else{
			if($arquivos){
				if($reg[$this->receitas_imagens])
					$arquivos = str_replace(",,", ",", $reg[$this->receitas_imagens].",".$arquivos);
					
				$coluna = ", $this->receitas_imagens   = '".$arquivos."'";
			}
			
			mysql_query("
			  UPDATE $this->tabela_receitas SET
				  $this->receitas_cod_categoria= '".$this->LimpaString($post[$this->receitas_cod_categoria])."'
				, $this->receitas_titulo       = '".$this->LimpaString($post[$this->receitas_titulo])."'
				, $this->receitas_descricao    = '".addslashes($post[$this->receitas_descricao])."'
				$coluna
			  WHERE $this->receitas_codigo = ".$post[$this->receitas_codigo]);
			
			$this->lista_receitas();
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}



//------------------------------------------------------------------------------------------------
	function mostra_receitas($categoria,$codigo=false){
		if(!$categoria){
			$categoria = mysql_fetch_array(mysql_query("SELECT codigo FROM categorias_receitas ORDER BY titulo ASC"));
			$categoria = $categoria['codigo'];
		}
        $sql = mysql_query("SELECT * FROM $this->tabela_receitas WHERE $this->receitas_cod_categoria = $categoria ORDER BY $this->receitas_codigo DESC");
		
		if(!$codigo){
			$reg_cod = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_receitas WHERE $this->receitas_cod_categoria = $categoria ORDER BY $this->receitas_codigo DESC"));
			$codigo = $reg_cod[$this->receitas_codigo];
		}
		
		if($codigo)
			$this->detalhes_receita($codigo);
		
		
		while($reg = mysql_fetch_array($sql)){
			//formata data
			$array = explode("-", $reg[$this->receitas_data_receita]);
			$reg[$this->receitas_data_receita] = $array[2]."/".$array[1]."/".$array[0];
			
			echo '<p><a href="receitas.php?c='.$categoria.'&id='.$reg[$this->receitas_codigo].'">'.$reg[$this->receitas_data_receita].' - '.$reg[$this->receitas_titulo].'</a></p>';
        }
		
		if(mysql_num_rows($sql) == 0){
			echo '
				<p class="destaque center">Nenhuma receita cadastrada.</p>';
		}
		
		
    }
	
//------------------------------------------------------------------------------------------------	
	function detalhes_receita($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_receitas WHERE $this->receitas_codigo = ".$this->anti_sql_injection($codigo)));
		
		//formata data
		$array = explode("-", $reg[$this->receitas_data_receita]);
		$reg[$this->receitas_data_receita] = $array[2]."/".$array[1]."/".$array[0];
		
		//busca a 1� imagem
		$arquivos = explode(",", $reg[$this->receitas_imagens]);
		
		$entrada = array(
			'%pacenta%',
			'%pernil%',
			'%lombo%',
			'%calabresa%',
			'%linguicapura%',
			'%bisteca%',
			'%filezinho%',
			'%alcatra%',
			'%costela%',
			'%copa%',
			'%su�%',
			'%rabo%',
			'%cabeca%',
			'%picanha%',
			'%lagarto%',
			'%cocaomole%',
			'%coxaoduro%',
			'%joelho%',
			'%pe%',
			'%patinho%' 
		);
		$saida = array(
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'placenta\' })">pacenta*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'pernil\' })">pernil*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'lombo\' })">lombo*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'calabresa\' })">calabresa*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'liguicapura\' })">lingui�a pura*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'bisteca\' })">bisteca*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'filezinho\' })">filezinho*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'alcatra\' })">alcatra*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'costela\' })">costela*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'copa\' })">copa*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'sua\' })">su�*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'rabo\' })">rabo*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'cabeca\' })">cabe�a*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'picanha\' })">picanha*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'lagarto\' })">lagarto*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'coxaomole\' })">cox�o mole*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'coxaoduro\' })">cox�o duro*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'joelho\' })">joelho*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'pe\' })">p�*</a>',
			'<a href="'.URLSITE.'/imagens/receitas.jpg" rel="highslide" onclick="return hs.expand(this,{ slideshowGroup: \'patinho\' })">patinho*</a>'
		);
		
		echo '<p>'.$reg[$this->receitas_data_receita].'</p>
			  <p>'.$reg[$this->receitas_titulo].'</p>
			  <div class="ck">'.str_replace($entrada, $saida,$reg[$this->receitas_descricao]).'</div>';
		
		if($reg[$this->receitas_imagens]){
			$contador = 0;
			echo '
			    <div class="row">';
			for($i=0; $i<count($arquivos); $i++){
				if($contador >= 3){
					echo '
			    </div>
			    <div class="row">';
					$contador = 0;
				}
				$contador++;
				
				echo '
				<div class="col-md-4">
                  <div class="post3">
				    <a href="'.URLADMIN.'/arquivos/receitas/'.$arquivos[$i].'" rel="highslide" onclick="return hs.expand(this)">
					  <img src="'.URLADMIN.'/arquivos/receitas/'.str_replace(".jpg","_mini.jpg", $arquivos[$i]).'"  alt=""/>
				    </a>
				  </div>
				</div>';
			}
			echo '
			    </div>
				';
		}
		
		/*echo '<div style="background:#fff; padding:10px; margin:10px 0;"> 
				<div id="fb-root"></div>
        		<script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6&appId=835833123227168";
                fjs.parentNode.insertBefore(js, fjs);
            		}(document, \'script\', \'facebook-jssdk\'));</script>
				<div class="fb-comments" data-href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'" data-numposts="10" data-width="100%"></div>
				</div>';*/
	}
	
}

?>
