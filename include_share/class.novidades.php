<?php
class novidades extends body{
    
    function __construct(){
        body :: __construct();
		novidades :: tabela_novidades();
    }

//------------------------------------------------------------------------------------------------
	function tabela_novidades(){
		$this->tabela_novidades       = 'novidades';
		$this->novidades_codigo       = 'codigo';
		$this->novidades_data_novidade= 'data_novidade';
		$this->novidades_titulo       = 'titulo';
		$this->novidades_descricao    = 'descricao';
		$this->novidades_imagens      = 'imagens';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo       = $this->novidades_codigo;
		$data_novidade= $this->novidades_data_novidade;
		$titulo       = $this->novidades_titulo;
		$descricao    = $this->novidades_descricao;
		$imagens      = $this->novidades_imagens;

		if($upd == true){
			$nome_tela   = "Alterar novidade";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_nov&codigo='.$_GET['codigo']."&atualizar=true";
		}else{
			$nome_tela   = "Nova novidade";
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_nov&inserir=true';
		}
		
		if($post[$data_novidade]){
			//formata data
			$array = explode("-", $post[$data_novidade]);
			$post[$data_novidade] = $array[2]."/".$array[1]."/".$array[0];
		}
		
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 500px; text-align: left; margin: auto;">';
		
		if($upd == true)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
		    <tr>
              <td width="15%"><label for="<?=$data_novidade;?>">Data *</label></td>
			  <td width="85%"><input name="<?=$data_novidade;?>" type="text" id="<?=$data_novidade;?>" value="<?=$post[$data_novidade];?>" maxlength="10" onkeypress="MascaraFormata(this, '##/##/####'); return sonum(event);" class="toolTip2" tooltip2="Informe uma data" /> exe: dd/mm/aaaa</td>
		    </tr>
            <tr>
              <td><label for="<?=$titulo;?>">Titulo *</label></td>
			  <td><input name="<?=$titulo;?>" type="text" id="<?=$titulo;?>" value="<?=$post[$titulo];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe o local do evento"/></td>
		    </tr>
		    <tr>
			  <td height="20"><label for="<?=$descricao;?>" style="margin-bottom: 2px;">Descri��o</label></td>
            </tr>
            <tr>
              <td colspan="2">
                <textarea name="<?=$descricao;?>" style="width: 100%; height: 200px;"><?=$post[$descricao];?></textarea>
                <script type="text/javascript">
				  CKEDITOR.replace( '<?=$descricao;?>',
					{
                       toolbar :
						[
							[ 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ]
						],
						height : 200,
						disableObjectResizing : true,
						removePlugins : 'smiley',
						filebrowserBrowseUrl : '../ckeditor/filebrowser.php?action=browse',
						filebrowserUploadUrl : '../ckeditor/filebrowser.php?action=upload'
				  } );
				</script>
              </td>
		    </tr>
<?php
		if($upd != false){
			$id = $post[$codigo];
			echo '
			<tr>
			  <td></td>
			  <td id="Imagens'.$id.'">';
			$arquivos = explode("," , $post[$imagens]);
			for($i=0; $i<count($arquivos); $i++){
			    if(!$post[$imagens]){
				    echo "<span style=\"color: #ff0000;\">Nenhuma imagem cadastrada.</span>";
				}else{
				    $ext = ".".end(explode(".",$arquivos[$i]));
					
					$arquivo = current(explode(".",$arquivos[$i]));
					$arquivo = strtr($arquivo, "_", " ");
					echo '
				<div style="margin-top: 3px;">
				  <a href="arquivos/novidades/'.$arquivos[$i].'" rel="highslides" onclick="return hs.expand(this)">'.$arquivo.$ext.'</a> - 
				  <a href="javascript: $(\'#Imagens'.$id.'\').load(\'admin.php?menu=cad_nov&acao=del_item&codigo='.$id.'&item='.$i.'\');" class="menu">Remover</a>
				</div>';
				}
		    }
			echo '
			  </td>
			</tr>';
		}
?>
			<tr>
              <td valign="top" style="padding-top: 8px;"><label for="<?=$imagens;?>">Imagens</label></td>
              <td>
                <input type="file" name="<?=$imagens;?>[]" size="25" style="width: 315px;" />
                
                <p id="Campos"></p>
				
				<a href="javascript: void(0);" onClick="NovoItem('imagens', 'Campos');" class="menu">Anexar outra imagem</a>
              </td>
            </tr>
		  </table>
		  <br />
		  <br />
		  <p>* Campos destacados s�o obrigat�rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_nov" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" class="bts" onclick="envia_novidade(this.form)" />
	      </center>
		</form>
        <?php
    }

//------------------------------------------------------------------------------------------------
    function lista_novidades($erro=false){
		$sql = mysql_query("SELECT * FROM $this->tabela_novidades ORDER BY $this->novidades_data_novidade DESC");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de novidades</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_nov&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Nova</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
        	      <td class="lista_tit cel_tabela" width="10%" align="center">Data</td>
        	      <td class="lista_tit cel_tabela" width="74%" align="left">T�tulo</td>
        	      <td class="lista_tit cel_tabela" colspan="3" width="16%" align="center">
				    <p style="width: 150px;">A��es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			//formata data
			$array = explode("-", $reg[$this->novidades_data_novidade]);
			$reg[$this->novidades_data_novidade] = $array[2]."/".$array[1]."/".$array[0];
			echo '
			    <tr>
                  <td class="cel_tabela" align="center">'.$reg[$this->novidades_data_novidade].'</td>
                  <td class="cel_tabela" align="left">'.$reg[$this->novidades_titulo].'</td>
                  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_nov&acao=edit&codigo='.$reg[$this->novidades_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_nov&acao=excl&codigo='.$reg[$this->novidades_codigo].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_novidade($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_novidades WHERE $this->novidades_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_novidade($codigo){
		$arq = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_novidades WHERE $this->novidades_codigo = ".$codigo));
		
		$arquivos = explode("," , $arq[$this->novidades_imagens]);
		
		for($i = 0; $i < count($arquivos); $i++){
			@unlink('arquivos/novidades/'.trim($arquivos[$i]));
			
			$ext = ".".end(explode(".", $arquivos[$i]));
			@unlink('arquivos/novidades/'.str_replace($ext, "_mini".$ext, trim($arquivos[$i])));
		}
		
		mysql_query("DELETE FROM $this->tabela_novidades WHERE $this->novidades_codigo = ".$codigo);
		
		$this->lista_novidades();
	}
	
//------------------------------------------------------------------------------------------------
	function exclui_item($id, $item){
		//consulta arquivos cadastrados
		$Consulta = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_novidades WHERE $this->novidades_codigo = ".$id));
		//gera array dos arquivos cadastrados
		$ArrayArquivos = explode(",", $Consulta[$this->novidades_imagens]);
		//deleta arquivo do disco
		@unlink("arquivos/novidades/".$ArrayArquivos[$item]);
		//Gera nome e remove do disco a img miniatura
		$ext = ".".end(explode(".", $ArrayArquivos[$item]));
		@unlink('arquivos/novidades/'.str_replace($ext, "_mini".$ext, trim($ArrayArquivos[$item])));
		
		//remove item do array
		unset($ArrayArquivos[$item]);
		//gera novo array de elementos
		$count = count($ArrayArquivos);
		for ($i = 0; $i <= $count; $i++) {
			if($ArrayArquivos[$i] != ""){
				//gera novo index pro array
				if(!$NovoIndexArray){ $NovoIndexArray = "1"; }
				if($NovoIndexArray == "1"){
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.$ArrayArquivos[$i];
				} else {
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.",".$ArrayArquivos[$i];
				}
			}
		}
		//imprime itens que nao foram removidos
		$arquivos = explode("," , $Arquivos);
		$NumeroArquivos = count($arquivos);
		if($arquivos[0] != ""){
			for($i=0;$i<$NumeroArquivos;$i++){
				$ext = ".".end(explode(".",$arquivos[$i]));
				$arquivo = current(explode(".",$arquivos[$i]));
				$arquivo = strtr($arquivo, "_", " ");
				echo '<div style="margin-top: 3px;">
						<a href="arquivos/novidades/'.$arquivos[$i].'" target="_blank">'.$arquivo.$ext.'</a> - 
						<a href="javascript: CarregaUrl(\'admin.php?menu=cad_nov&acao=del_item&codigo='.$id.'&item='.$i.'\', \'Imagens'.$id.'\');" class="menu">Remover</a>
					  </div>';
			}
		}else{
			echo '<span style="color: #ff0000;">Nenhuma imagem cadastrada.</span>';
		}
		//grava altera�oes no sql
		mysql_query("UPDATE $this->tabela_novidades SET $this->novidades_imagens='".$Arquivos."' WHERE $this->novidades_codigo = ".$id);
	}

//------------------------------------------------------------------------------------------------
	function insere_novidade($post){
		//formata data
		$array = explode("/", $post[$this->novidades_data_novidade]);
		$post[$this->novidades_data_novidade] = $array[2]."-".$array[1]."-".$array[0];
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->novidades_imagens], "arquivos/novidades/", true);
		
		if($arquivos == "invalido"){
			$this->imprime_form($post, false, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			exit();
			
		}else{
			mysql_query("
			  INSERT INTO $this->tabela_novidades (
				  $this->novidades_data_novidade
				, $this->novidades_titulo
				, $this->novidades_descricao
				, $this->novidades_imagens
			  )VALUES (
				  '".$post[$this->novidades_data_novidade]."'
				, '".$this->LimpaString($post[$this->novidades_titulo])."'
				, '".$post[$this->novidades_descricao]."'
				, '".$arquivos."'
			  )");
			
			if(mysql_error()){
				$array = explode("," , $arquivos);
		        
				for($i=0; $i<count($array); $i++){
					@unlink('arquivos/novidades/'.trim($array[$i]));
					
					$ext = ".".end(explode(".", $array[$i]));
					$arq_mini = str_replace($ext, "_mini".$ext, trim($array[$i]));
					@unlink('arquivos/novidades/'.trim($arq_mini));
				}
				
				$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.<br>".mysql_error());
				exit();
				
            }else{
				$this->lista_novidades();
				
			}
		}
    }

//------------------------------------------------------------------------------------------------
	function atualiza_novidade($post){
		//formata data
		$array = explode("/", $post[$this->novidades_data_novidade]);
		$post[$this->novidades_data_novidade] = $array[2]."-".$array[1]."-".$array[0];
		
		//seleciona banco
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_novidades 
		         WHERE codigo = ".$post[$this->novidades_codigo]));
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->novidades_imagens], "arquivos/novidades/", true);
		
		if($arquivos == "invalido"){
			$post[$this->novidades_imagens] = $reg[$this->novidades_imagens];
			$this->imprime_form($post, true, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			
			exit;
		}else{
			if($arquivos){
				if($reg[$this->novidades_imagens])
					$arquivos = str_replace(",,", ",", $reg[$this->novidades_imagens].",".$arquivos);
					
				$coluna = ", $this->novidades_imagens   = '".$arquivos."'";
			}
			
			mysql_query("
			  UPDATE $this->tabela_novidades SET
				  $this->novidades_data_novidade = '".$post[$this->novidades_data_novidade]."'
				, $this->novidades_titulo       = '".$this->LimpaString($post[$this->novidades_titulo])."'
				, $this->novidades_descricao    = '".$post[$this->novidades_descricao]."'
				$coluna
			  WHERE $this->novidades_codigo = ".$post[$this->novidades_codigo]);
			
			$this->lista_novidades();
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}


//------------------------------------------------------------------------------------------------	
	function mostra_home(){
		$sql = mysql_query("SELECT * FROM $this->tabela_novidades ORDER BY data_novidade DESC LIMIT 2");
		
		echo '<table width="100%" cellpadding="0" cellspacing="0">
		        <tr>
				  <td align="left"><p class="destaque preto f18">NOVIDADES</p></td>
				  <td align="right"><a href="#novidades" rel="novidades.php?menu=ok" class="link destaque f10 vermelho">Veja mais <img src="imagens/seta.png" aling="absmiddle" /></a></td>
				</tr>
			  </table>  
		      <table width="100%" cellpadding="0" cellspacing="0">
			    <tr>
				  </td height="10">&nbsp;</td>
				</tr>';
		
		while($reg = mysql_fetch_array($sql)){
			
			$data = explode("-",$reg[$this->novidades_data_novidade]);

			echo $aux.'
			    <tr>
				  <td valign="top">
					<a href="#novidades" rel="novidades.php?menu=ok&codigo='.$reg[$this->novidades_codigo].'" class="link cinza"><p class="f14 destaque">'.$data[2].'/'.$data[1].'/'.$data[0].' - '.$reg[$this->novidades_titulo].'</p></a>
					<div class="justify f12" style="margin-left:10px;">'.strip_tags(nl2br($this->limita_str($reg[$this->novidades_descricao],300))).'</div>
				  </td>
				</tr>'; 
			
			$aux = '
			    <tr>
				  <td height="10">&nbsp;</td>
				</tr>
				<tr>
				  <td style="border-top:1px solid #c9c9c9"></td>
				</tr>
				<tr>
				  <td height="10">&nbsp;</td>
				</tr>';	 
		}
		
		echo '</table>';
		  
	}

//------------------------------------------------------------------------------------------------
	function mostra_novidades($codigo=false){
        $sql = mysql_query("SELECT * FROM $this->tabela_novidades ORDER BY $this->novidades_data_novidade DESC");
		
		echo '<table width="100%" cellspacing="5" style="margin: auto;">';
		
		while($reg = mysql_fetch_array($sql)){
			//formata data
			$array = explode("-", $reg[$this->novidades_data_novidade]);
			$reg[$this->novidades_data_novidade] = $array[2]."/".$array[1]."/".$array[0];
			
			if(!$mostra){
				$mostra = true;
				
				if(!is_numeric($codigo))
					$codigo = $reg[$this->novidades_codigo];
				
				$this->detalhes_novidade($codigo);
				echo '
				<tr>
				  <td colspan="4" class="destaque vermelho f14" style="padding-top: 20px; border-bottom: 1px solid #cecece;">Mais not&iacute;cias</td>
				</tr>';
			}
			echo '
				<tr>
			      <td colspan="4"> 
				    <a href="#novidades" rel="novidades.php?menu=ok&codigo='.$reg[$this->novidades_codigo].'" class="link destaque cinza">'.$reg[$this->novidades_data_novidade].' - '.$reg[$this->novidades_titulo].'
				    </a>
				  </td>
				</tr>';
        }
		
		if(mysql_num_rows($sql) == 0){
			echo '
				<tr>
				  <td class="destaque" >Nenhuma novidade cadastrada.</td>
				</tr>';
		}
		
		echo '</table>';
    }
	
//------------------------------------------------------------------------------------------------	
	function detalhes_novidade($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_novidades WHERE $this->novidades_codigo = ".$this->anti_sql_injection($codigo)));
		
		//formata data
		$array = explode("-", $reg[$this->novidades_data_novidade]);
		$reg[$this->novidades_data_novidade] = $array[2]."/".$array[1]."/".$array[0];
		
		//busca a 1� imagem
		$arquivos = explode(",", $reg[$this->novidades_imagens]);
		
		echo '	<tr>
			      <td colspan="4" class="f11">'.$reg[$this->novidades_data_novidade].'</td>
				</tr>
				<tr>
			      <td colspan="4" class="destaque vermelho f14">'.$reg[$this->novidades_titulo].'</td>
				</tr>
				<tr>
			      <td colspan="4" style="padding: 5px 0 10px 0;"><div class="ck">'.$reg[$this->novidades_descricao].'</div></td>
				</tr>
				<tr>';
		
		if($reg[$this->novidades_imagens]){
			$contador = 0;
			for($i=0; $i<count($arquivos); $i++){
				if($contador >= 4){
					echo '
			    </tr>
			    <tr>';
					$contador = 0;
				}
				$contador++;
				
				echo '
				  <td width="25%" align="left" style="padding: 5px;">
				    <a href="'.URLADMIN.'/arquivos/novidades/'.$arquivos[$i].'" rel="highslide" onclick="return hs.expand(this)">
					  <img src="'.URLADMIN.'/arquivos/novidades/'.str_replace(".jpg","_mini.jpg", $arquivos[$i]).'"  alt="" style="border:3px solid #fff45d;"/>
				    </a>
				  </td>';
			}
			echo '
			    </tr>';
		}
	}
	
}

?>
