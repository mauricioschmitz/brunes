<?php
class acessos extends body{
    
    function __construct(){
        body :: __construct();
		acessos :: tabela_acessos();
    }
	
//------------------------------------------------------------------------------------------------
	function tabela_acessos(){
		$this->tabela_acessos = 'usuarios';
		$this->acessos_codigo = 'codigo';
		$this->acessos_nome   = 'nome';
		$this->acessos_email  = 'email';
		$this->acessos_login  = 'login';
		$this->acessos_senha  = 'senha';
	}
	
//------------------------------------------------------------------------------------------------
    function form_config($post=false, $upd=true, $erro=false, $retorno=false){
		$codigo           = $this->acessos_codigo;
		$email            = $this->acessos_email;
		$senha            = $this->acessos_senha;
		$nome             = $this->acessos_nome;
		$login            = $this->acessos_login;
		if($upd == true){
			$nome_tela   = "Alterar configura&ccedil;&otilde;es";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_config&acao=config&atualizar=true';
		}
				
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center;">
          '.$nome_tela.'
        </p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 540px; text-align: left; margin: auto;">';
		
		if($erro)
			echo '<div class="erro" style="margin-top:5px;">'.$erro.'</div><script>mostra_erro();</script>';

		if($retorno)
			echo '<div class="retorno" style="margin-top:5px;">'.$retorno.'</div><script>mostra_retorno();</script>';


		if($upd == true)
			echo '<input type="hidden" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
            <tr>
              <td align="right"><label for="<?=$nome;?>">Nome *</label></td>
			  <td><input name="<?=$nome;?>" type="text" id="<?=$nome;?>" value="<?=$post[$nome];?>" maxlength="200" style="width:312px;" class="toolTip2" tooltip2="Informe seu nome"/></td>
		    </tr>
            <tr>
              <td align="right"><label for="<?=$email;?>">E-mail *</label></td>
			  <td><input name="<?=$email;?>" type="email" id="<?=$email;?>" value="<?=$post[$email];?>" maxlength="200" style="width:312px;" placeholder="email@servidor.com"   class="toolTip2" tooltip2="Informe o email"/></td>
		    </tr>
            <tr>
              <td align="right"><label for="<?=$login;?>">Login *</label></td>
			  <td><input name="<?=$login;?>" type="text" id="<?=$login;?>" value="<?=$post[$login];?>" maxlength="200" style="width:312px;" class="toolTip2" tooltip2="Informe seu login"/></td>
		    </tr>
            <tr>
              <td align="right"><label for="<?=$senha;?>">Senha </label></td>
			  <td><input name="<?=$senha;?>" type="password" id="<?=$senha;?>" value="" maxlength="200" style="width:120px;"  class="toolTip2" tooltip2="Informe uma senha"/> Obs: Preencha se desejar alterar</td>
		    </tr>
		  </table>
          <br />
          <center>
		    <input type="button" class="bts" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" onclick="envia_config(this.form)" />
	      </center>
		</form>
        <?php
		
    }	
	
//------------------------------------------------------------------------------------------------
	function atualiza_acesso($post){
		
		//verifica disponibilidade de login
		$reg_login = mysql_num_rows(mysql_query("SELECT * FROM $this->tabela_acessos
					   WHERE login = '".trim($this->LimpaString($post['login']))."' AND codigo <> ".$post['codigo']));
		
		if($reg_login > 0){
			$this->form_config($post, true, "Login j� cadastrado. Por favor escolha outro.");
			exit;
		}else if(strlen(trim($this->LimpaString($post['login']))) < 5){
			$this->form_config($post, true, "Seu Login deve ter 5 ou mais caracteres.");
			exit();
		}else if(!$this->VerificarLoginSenha(trim($this->LimpaString($_POST['login'])))){
			$this->form_config($post, true, "Seu Login deve ter somente letras, n�meros, - e _.");
			exit();
		}else if(!$this->VerificarLoginSenha(trim($this->LimpaString($_POST['senha'])))){
			$this->form_config($post, true, "Sua Senha deve ter somente letras, n�meros, - e _.");
			exit();
		}else{
			if($post[$this->acessos_senha]){
			    $col_senha = ", $this->acessos_senha = '".base64_encode(md5(trim($this->LimpaString($post[$this->acessos_senha]))))."'";
				if(strlen(trim($this->LimpaString($_POST['senha']))) < 6){
					$this->form_config($post, true, "Sua Senha deve ter 6 ou mais caracteres.");
					exit();
				}
			}
			
			mysql_query("UPDATE $this->tabela_acessos SET
				  $this->acessos_nome  = '".$this->LimpaString($post[$this->acessos_nome])."'
				, $this->acessos_email = '".$this->LimpaString($post[$this->acessos_email])."'  
				, $this->acessos_login = '".trim($this->LimpaString($post[$this->acessos_login]))."'
				$col_senha
			  WHERE $this->acessos_codigo = ".$post[$this->acessos_codigo]);
			
			$reg = $this->get_acesso($_SESSION['USUARIO_ID']);
			
			if(mysql_error()){
				$this->form_config($reg, true, 'Ocorreu um erro ao processar a tarefa, tente novamente');
			}else{
				$this->form_config($reg, true,false, 'Dados alterados com sucesso');
			}
			
		}
		
	}
	
//------------------------------------------------------------------------------------------------
	function insere_acesso($post){
		//verifica disponibilidade de login
		$reg_login = mysql_num_rows(mysql_query("SELECT * FROM $this->tabela_acessos 
					   WHERE (login = '".trim($this->LimpaString($post['login']))."')"));
		
		if($reg_login > 0){
			$this->imprime_form($post, false, "Login j� cadastrado. Por favor escolha outro.");
			exit;
		}else if(strlen(trim($this->LimpaString($post['login']))) < 5){
			$this->imprime_form($post, false, "Seu Login deve ter 5 ou mais caracteres.");
			exit();
		}else if(strlen(trim($this->LimpaString($_POST['senha']))) < 6){
			$this->imprime_form($post, false, "Sua Senha deve ter 6 ou mais caracteres.");
			exit();
		}else if(!$this->VerificarLoginSenha(trim($this->LimpaString($_POST['login'])))){
			$this->imprime_form($post, false, "Seu Login deve ter somente letras, n�meros, - e _.");
			exit();
		}else if(!$this->VerificarLoginSenha(trim($this->LimpaString($_POST['senha'])))){
			$this->imprime_form($post, false, "Sua Senha deve ter somente letras, n�meros, - e _.");
			exit();
		}else{
			mysql_query("INSERT INTO $this->tabela_acessos (
			    $this->acessos_nome
			  , $this->acessos_email
			  , $this->acessos_login
			  , $this->acessos_senha
			) VALUES (
				'".$this->LimpaString($post[$this->acessos_nome])."'
			  , '".$this->LimpaString($post[$this->acessos_email])."'	
			  , '".trim($this->LimpaString($post[$this->acessos_login]))."'
			  , '".md5(base64_encode(trim($this->LimpaString($post[$this->acessos_senha]))))."'
			)");
			
			$reg = $this->get_acesso($_SESSION['USUARIO_ID']);
			$this->imprime_form($reg, true);
		}
	}
	
//------------------------------------------------------------------------------------------------
	function get_acesso($cod){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_acessos 
		  WHERE $this->acessos_codigo = ".$cod));

		return $reg;
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
	}

//------------------------------------------------------------------------------------------------	
	function VerificarLoginSenha($str) {
		return eregi("^[_0-9a-z-]*$", $str);
	}
	
}

/*

*/
?>