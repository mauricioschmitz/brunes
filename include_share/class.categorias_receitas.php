<?php
class categorias_receitas extends body{
    
    function __construct(){
        body :: __construct();
		categorias_receitas :: tabela_categorias_receitas();
    }

//------------------------------------------------------------------------------------------------
	function tabela_categorias_receitas(){
		$this->tabela_categorias_receitas       = 'categorias_receitas';
		$this->categorias_receitas_codigo       = 'codigo';
		$this->categorias_receitas_titulo       = 'titulo';
		$this->categorias_receitas_ativo        = 'ativo';
		$this->categorias_receitas_imagens      = 'imagens';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo       = $this->categorias_receitas_codigo;
		$titulo       = $this->categorias_receitas_titulo;
		$imagens      = $this->categorias_receitas_imagens;

		if($upd == true){
			$nome_tela   = "Alterar categoria";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_cat_rec&codigo='.$_GET['codigo']."&atualizar=true";
		}else{
			$nome_tela   = "Nova categoria";
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_cat_rec&inserir=true';
		}
		
		if($post[$data_categoria]){
			//formata data
			$array = explode("-", $post[$data_categoria]);
			$post[$data_categoria] = $array[2]."/".$array[1]."/".$array[0];
		}
		
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 500px; text-align: left; margin: auto;">';
		
		if($upd == true)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
		    <tr>
              <td><label for="<?=$titulo;?>">Titulo *</label></td>
			  <td><input name="<?=$titulo;?>" type="text" id="<?=$titulo;?>" value="<?=$post[$titulo];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe um t�tulo"/></td>
		    </tr>
<?php
		if($upd != false){
			$id = $post[$codigo];
			echo '
			<tr>
			  <td></td>
			  <td id="Imagens'.$id.'">';
			$arquivos = explode("," , $post[$imagens]);
			for($i=0; $i<count($arquivos); $i++){
			    if(!$post[$imagens]){
				    echo "<span style=\"color: #ff0000;\">Nenhuma imagem cadastrada.</span>";
				}else{
				    $ext = ".".end(explode(".",$arquivos[$i]));
					
					$arquivo = current(explode(".",$arquivos[$i]));
					$arquivo = strtr($arquivo, "_", " ");
					echo '
				<div style="margin-top: 3px;">
				  <a href="arquivos/categorias_receitas/'.$arquivos[$i].'" rel="highslides" onclick="return hs.expand(this)">'.$arquivo.$ext.'</a> - 
				  <a href="javascript: $(\'#Imagens'.$id.'\').load(\'admin.php?menu=cad_cat_rec&acao=del_item&codigo='.$id.'&item='.$i.'\');" class="menu">Remover</a>
				</div>';
				}
		    }
			echo '
			  </td>
			</tr>';
		}
?>
			<tr>
              <td valign="top" style="padding-top: 8px;"><label for="<?=$imagens;?>">Imagens</label></td>
              <td>
                <input type="file" name="<?=$imagens;?>[]" size="25" style="width: 315px;" />
                
                <p id="Campos"></p>
				
				<a href="javascript: void(0);" onClick="NovoItem('imagens', 'Campos');" class="menu">Anexar outra imagem</a>
              </td>
            </tr>
		  </table>
		  <br />
		  <br />
		  <p>* Campos destacados s&atilde;o obrigat&oacute;rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_cat_rec" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" class="bts" onclick="envia_categoria(this.form)" />
	      </center>
		</form>
        <?php
    }

//------------------------------------------------------------------------------------------------
    function lista_categorias_receitas($erro=false){
		$sql = mysql_query("SELECT * FROM $this->tabela_categorias_receitas WHERE ativo ='sim' ORDER BY $this->categorias_receitas_titulo");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de categorias_receitas</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_cat_rec&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Nova</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
        	      <td class="lista_tit cel_tabela" width="84%" align="left">T&iacute;tulo</td>
        	      <td class="lista_tit cel_tabela" colspan="3" width="16%" align="center">
				    <p style="width: 150px;">A&ccedil;&otilde;es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			//formata data
			echo '
			    <tr>
                  <td class="cel_tabela" align="left">'.$reg[$this->categorias_receitas_titulo].'</td>
                  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_cat_rec&acao=edit&codigo='.$reg[$this->categorias_receitas_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_cat_rec&acao=excl&codigo='.$reg[$this->categorias_receitas_codigo].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_categoria($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_categorias_receitas WHERE $this->categorias_receitas_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_categoria($codigo){
		$arq = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_categorias_receitas WHERE $this->categorias_receitas_codigo = ".$codigo));
		
		$arquivos = explode("," , $arq[$this->categorias_receitas_imagens]);
		
		for($i = 0; $i < count($arquivos); $i++){
			@unlink('arquivos/categorias_receitas/'.trim($arquivos[$i]));
			
			$ext = ".".end(explode(".", $arquivos[$i]));
			@unlink('arquivos/categorias_receitas/'.str_replace($ext, "_mini".$ext, trim($arquivos[$i])));
		}
		
		mysql_query("UPDATE $this->tabela_categorias_receitas SET ativo = 'nao' WHERE $this->categorias_receitas_codigo = ".$codigo);
		
		$this->lista_categorias_receitas();
	}
	
//------------------------------------------------------------------------------------------------
	function exclui_item($id, $item){
		//consulta arquivos cadastrados
		$Consulta = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_categorias_receitas WHERE $this->categorias_receitas_codigo = ".$id));
		//gera array dos arquivos cadastrados
		$ArrayArquivos = explode(",", $Consulta[$this->categorias_receitas_imagens]);
		//deleta arquivo do disco
		@unlink("arquivos/categorias_receitas/".$ArrayArquivos[$item]);
		//Gera nome e remove do disco a img miniatura
		$ext = ".".end(explode(".", $ArrayArquivos[$item]));
		@unlink('arquivos/categorias_receitas/'.str_replace($ext, "_mini".$ext, trim($ArrayArquivos[$item])));
		
		//remove item do array
		unset($ArrayArquivos[$item]);
		//gera novo array de elementos
		$count = count($ArrayArquivos);
		for ($i = 0; $i <= $count; $i++) {
			if($ArrayArquivos[$i] != ""){
				//gera novo index pro array
				if(!$NovoIndexArray){ $NovoIndexArray = "1"; }
				if($NovoIndexArray == "1"){
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.$ArrayArquivos[$i];
				} else {
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.",".$ArrayArquivos[$i];
				}
			}
		}
		//imprime itens que nao foram removidos
		$arquivos = explode("," , $Arquivos);
		$NumeroArquivos = count($arquivos);
		if($arquivos[0] != ""){
			for($i=0;$i<$NumeroArquivos;$i++){
				$ext = ".".end(explode(".",$arquivos[$i]));
				$arquivo = current(explode(".",$arquivos[$i]));
				$arquivo = strtr($arquivo, "_", " ");
				echo '<div style="margin-top: 3px;">
						<a href="arquivos/categorias_receitas/'.$arquivos[$i].'" target="_blank">'.$arquivo.$ext.'</a> - 
						<a href="javascript: CarregaUrl(\'admin.php?menu=cad_cat_rec&acao=del_item&codigo='.$id.'&item='.$i.'\', \'Imagens'.$id.'\');" class="menu">Remover</a>
					  </div>';
			}
		}else{
			echo '<span style="color: #ff0000;">Nenhuma imagem cadastrada.</span>';
		}
		//grava altera�oes no sql
		mysql_query("UPDATE $this->tabela_categorias_receitas SET $this->categorias_receitas_imagens='".$Arquivos."' WHERE $this->categorias_receitas_codigo = ".$id);
	}

//------------------------------------------------------------------------------------------------
	function insere_categoria($post){
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->categorias_receitas_imagens], "arquivos/categorias_receitas/", true);
		
		if($arquivos == "invalido"){
			$this->imprime_form($post, false, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			exit();
			
		}else{
			mysql_query("
			  INSERT INTO $this->tabela_categorias_receitas (
				  $this->categorias_receitas_titulo
				, $this->categorias_receitas_ativo
				, $this->categorias_receitas_imagens
			  )VALUES (
				  '".$this->LimpaString($post[$this->categorias_receitas_titulo])."'
				, 'sim'
				, '".$arquivos."'
			  )");
			
			if(mysql_error()){
				$array = explode("," , $arquivos);
		        
				for($i=0; $i<count($array); $i++){
					@unlink('arquivos/categorias_receitas/'.trim($array[$i]));
					
					$ext = ".".end(explode(".", $array[$i]));
					$arq_mini = str_replace($ext, "_mini".$ext, trim($array[$i]));
					@unlink('arquivos/categorias_receitas/'.trim($arq_mini));
				}
				
				$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.<br>".mysql_error());
				exit();
				
            }else{
				$this->lista_categorias_receitas();
				
			}
		}
    }

//------------------------------------------------------------------------------------------------
	function atualiza_categoria($post){
		//seleciona banco
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_categorias_receitas 
		         WHERE codigo = ".$post[$this->categorias_receitas_codigo]));
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->categorias_receitas_imagens], "arquivos/categorias_receitas/", true);
		
		if($arquivos == "invalido"){
			$post[$this->categorias_receitas_imagens] = $reg[$this->categorias_receitas_imagens];
			$this->imprime_form($post, true, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			
			exit;
		}else{
			if($arquivos){
				if($reg[$this->categorias_receitas_imagens])
					$arquivos = str_replace(",,", ",", $reg[$this->categorias_receitas_imagens].",".$arquivos);
					
				$coluna = ", $this->categorias_receitas_imagens   = '".$arquivos."'";
			}
			
			mysql_query("
			  UPDATE $this->tabela_categorias_receitas SET
				  $this->categorias_receitas_titulo       = '".$this->LimpaString($post[$this->categorias_receitas_titulo])."'
				$coluna
			  WHERE $this->categorias_receitas_codigo = ".$post[$this->categorias_receitas_codigo]);
			
			$this->lista_categorias_receitas();
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}
	
}

?>
