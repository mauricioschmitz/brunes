<?php
    //gera imagem e 'tb_'miniatura com $tamanho por tamanho relativo, nome dos arquivos retorna com ','
	function uploadFotos($tamanho, $arquivos, $dir_salva, $thumb=true){
		$count = count($arquivos['name']);
		for ($i = 0; $i < $count; $i++) {
			if($arquivos['name'][$i] != ""){
				$nome = $arquivos['name'][$i];
				
				$ext = explode(".",$nome);
				$qt = count($ext);
				$ext = strtolower($ext[$qt-1]);
				
				if ($ext != 'jpg' && $ext != 'jpeg') {
					return "invalido";
				}
				
			}
		}
		
		for ($i = 0; $i < $count; $i++) {
			if($arquivos['name'][$i] != ""){
				//nome completo do arquivo NOMEDOARQUIVO-.-SESSAO_DATA_HORA_NUMEROFOR-.-EXTENSAO
				$nome = strtolower(GeraNomeArquivo($arquivos['name'][$i]));
				$ext = explode(".",$nome);
				$qt = count($ext);
				$ext = ".".$ext[$qt-1];
				$arquivo = current(explode(".",$nome));
				$nome = $arquivo.".".date ("j_m_Y")."_".date ("H_i_s").$ext;
				//insere o primeiro item sem virgula
				if($i == 0){
					$Arquivos = $Arquivos.$nome;
				} else {
					$Arquivos = $Arquivos.",".$nome;
				}
				//IMAGEM
				$medida = getimagesize($arquivos['tmp_name'][$i]);
				if($medida[0]>$medida[1]) {
					if($tamanho > $medida[0]){
						$x = $medida[0];
						if($tamanho > $medida[1]){
							$y = $medida[1];
						}
					}else{
						$x = $tamanho;
						$y = $medida[1]/($medida[0]/$tamanho);
					}
					
					$tx = 350;
					$ty = $medida[1]/($medida[0]/350);
				}else{
					if($tamanho > $medida[1]){
						$y = $medida[1];
						if($tamanho > $medida[0]){
							$x = $medida[0];
						}
					}else{
						$y = $tamanho;
						$x = $medida[0]/($medida[1]/$tamanho);
					}
					
					$ty = 350;
					$tx = $medida[0]/($medida[1]/350);
				}
				//cria imagem temporarioa
				$imagem = imagecreatefromjpeg($arquivos['tmp_name'][$i]);
				
				//imagem
				$final = imagecreatetruecolor($x, $y);
				imagecopyresampled($final, $imagem,0, 0, 0, 0, $x, $y, $medida[0], $medida[1]);
				imagejpeg($final, $dir_salva.$nome, 90);
				imagedestroy($final);
				
				if($thumb){
					//thumbnail
					$thumb = imagecreatetruecolor($tx, $ty);
					imagecopyresampled($thumb, $imagem, 0, 0, 0, 0, $tx, $ty, $medida[0], $medida[1]);
					$ext = explode(".",$nome);
					$qt = count($ext);
					$ext = ".".$ext[$qt-1];
					
					imagejpeg($thumb, $dir_salva.str_replace($ext,"_mini".$ext, $nome), 90);
					imagedestroy($thumb);
				}
			}
		}
		return $Arquivos;
    }
	
	function UploadArquivos($Upload, $diretorio){
		$count = count($Upload['name']);
		for ($i = 0; $i < $count; $i++) {
			//se o campo arquivo nao for vazio
			if($Upload['name'][$i] != ""){
				//nome completo do arquivo NOMEDOARQUIVO-.-SESSAO_DATA_HORA_NUMEROFOR-.-EXTENSAO
				$nome = strtolower(GeraNomeArquivo($Upload['name'][$i]));
				$ext = explode(".",$nome);
				$qt = count($ext);
				$ext = ".".$ext[$qt-1];
				$arquivo = current(explode(".",$nome));
				$nome = $arquivo.".".date("Y"."-"."m"."-"."d")."_".date("H"."-"."i"."-"."s").$ext;
				//insere o primeiro item sem virgula
				if($i == 0){
					$Arquivos = $Arquivos.$nome;
				} else {
					$Arquivos = $Arquivos.",".$nome;
				}
				move_uploaded_file($Upload['tmp_name'][$i], $diretorio.$nome);
			}
		}
		return $Arquivos;
	}
	
	function GeraNomeArquivo($str){
		//pega extensao do arquivo
		$ext = explode(".",$str);
		$qt = count($ext);
		$ext = ".".$ext[$qt-1];
		//pega nome at� o primeiro ponto
		$arquivo = substr($str, 0, -strlen($ext));
		//array de caracteres invalidos
		$de   = "��������������,.;�`' ";
		//array de caracteres para substituir
		$para = "aaaaeeiooouucy_______";
		//remove caracteres invalidos
		$str = strtr($arquivo, $de, $para).$ext;
		//retorna valor
		return $str;
	}
?>