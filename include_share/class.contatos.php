<?php
class contatos extends body{
	function imprime_form($post=false, $msg=array('','',array())){
?>
<div class="row">
	<div class="col-md-offset-3 col-md-6">
        <p>Voc&ecirc; pode usar o formul&aacute;rio abaixo ou se preferir mande um e-mail para <b><?=EMAIL;?></b></p>
        <div class="<?=($msg[0]?'':'done');?>">
            <div class="alert alert-<?=$msg[0];?>">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <?=$msg[1];?>
            </div>
        </div>
        <div class="contact-form">
                                                
            <form method="post" action="contato.php" id="contactform" class="contact">
                <div class="form-group has-feedback <?php if($msg[0]){echo (in_array('nome', $msg[2])?'has-error':'has-success'); };?>">
                    <label for="name">Name*</label>
                    <input type="text" class="form-control" name="nome" value="<?=$post['nome'];?>" placeholder="">
                    <i class="fa fa-user form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback <?php if($msg[0]){echo (in_array('telefone', $msg[2])?'has-error':'has-success'); };?>">
                    <label for="telefone">Telefone*</label>
                    <input type="text" class="form-control telefone" id="telefone" name="telefone" value="<?=$post['telefone'];?>" placeholder="" maxlength="15" onkeyup="mascara(document.getElementById('telefone'), mtel)">
                    <i class="fa fa-envelope form-control-feedback"></i>
                </div>
    
                <div class="form-group has-feedback <?php if($msg[0]){echo (in_array('email', $msg[2])?'has-error':'has-success'); };?>">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" value="<?=$post['email'];?>" placeholder="">
                    <i class="fa fa-envelope form-control-feedback"></i>
                </div>
    
                <div class="form-group has-feedback <?php if($msg[0]){echo (in_array('mensagem', $msg[2])?'has-error':'has-success'); };?>">
                    <label for="message">Message*</label>
                    <textarea class="form-control" rows="6" name="mensagem" placeholder=""><?=$post['mensagem'];?></textarea>
                    <i class="fa fa-pencil form-control-feedback"></i>
                </div>
                <input type="submit" value="Submit" id="submit" class="submit btn btn-default">
            </form>
             
            
        </div>
    </div>
</div>    
<script type="text/javascript">
    

function mascara(o, f) {
    v_obj = o;
    v_fun = f;
    setTimeout(execmascara(), 1);
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value);
}

function mtel(v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

</script>
<?php

    }
    
//------------------------------------------------------------------------------------------------
    function envia_email($post){
		if(!$post['nome'] || !$post['telefone'] || !$post['mensagem']){
			$campos = array();
			if(!$post['nome'])
				array_push($campos,'nome');
			if(!$post['telefone'])
				array_push($campos,'telefone');
			if(!$post['mensagem'])
				array_push($campos,'mensagem');
			$this->imprime_form($post, array('danger','Preencha todos os campos.', $campos));
		}else{
			$corpo = "<strong>Brune`s Indústria de Carnes e Defumados</strong><br /><br />
					  <strong>Nome:</strong> ".$post['nome']." <br />
                      <strong>Telefone:</strong> ".$post['telefone']." <br />
                      <strong>E-mail:</strong> ".$post['email']." <br />
					  <strong>Mensagem:</strong> ".nl2br($post['mensagem'])." <br />";
			
			if($this->dispara_email($post['nome'],(isset($post['email']) && $post['email'] != '' ? $post['email'] : EMAIL),$post['nome'],EMAIL,'Contato enviado pelo site',$corpo)){
				unset($_POST);
				$this->imprime_form(false,array('success','Seu contato foi enviado com sucesso.',array()));
				
			}else{
				$this->imprime_form($post, array('success','Ocorreu um erro inesperado com o envio do e-mail. Por favor tente novamente.',array()));
				
			}
		}
    }
    
}
?>
