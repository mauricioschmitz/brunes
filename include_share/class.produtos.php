<?php
class produtos extends body{
    
    function __construct(){
        body :: __construct();
		produtos :: tabela_produtos();
    }

//------------------------------------------------------------------------------------------------
	function tabela_produtos(){
		$this->tabela_produtos       = 'produtos';
		$this->produtos_codigo       = 'codigo';
		$this->produtos_cod_categoria= 'cod_categoria';
		$this->produtos_titulo       = 'titulo';
		$this->produtos_descricao    = 'descricao';
		$this->produtos_imagens      = 'imagens';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo       = $this->produtos_codigo;
		$cod_categoria= $this->produtos_cod_categoria;
		$titulo       = $this->produtos_titulo;
		$descricao    = $this->produtos_descricao;
		$imagens      = $this->produtos_imagens;

		if($upd == true){
			$nome_tela   = "Alterar produto";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_pro&codigo='.$_GET['codigo']."&atualizar=true";
		}else{
			$nome_tela   = "Nova produto";
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_pro&inserir=true';
		}
		
		if($post[$data_produto]){
			//formata data
			$array = explode("-", $post[$data_produto]);
			$post[$data_produto] = $array[2]."/".$array[1]."/".$array[0];
		}
		
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 500px; text-align: left; margin: auto;">';
		
		if($upd == true)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
		    <tr>
              <td width="15%"><label for="<?=$cod_categoria;?>">Categoria *</label></td>
			  <td width="85%">
			    <select name="<?=$cod_categoria;?>" id="categoria">
<?php 
		$sql_gru = mysql_query("SELECT * FROM categorias WHERE ativo ='sim' ORDER BY titulo");
		
		while($reg_gru = mysql_fetch_array($sql_gru)){
			if($post[$cod_categoria] == $reg_gru['codigo']){
				$select = "selected";
			}
			
			echo '<option value="'.$reg_gru['codigo'].'" '.$select.'>'.$reg_gru['titulo'].'</option>';
			$select = "";
		}
?>
			    </select>
		  	  </td>
		    </tr>
            <tr>
              <td><label for="<?=$titulo;?>">Titulo *</label></td>
			  <td><input name="<?=$titulo;?>" type="text" id="<?=$titulo;?>" value="<?=$post[$titulo];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe um t�tulo"/></td>
		    </tr>
		    <tr>
			  <td height="20"><label for="<?=$descricao;?>" style="margin-bottom: 2px;">Descri&ccedil;&atilde;o</label></td>
            </tr>
            <tr>
              <td colspan="2">
                <textarea name="<?=$descricao;?>" style="width: 100%; height: 200px;"><?=$post[$descricao];?></textarea>
                <script type="text/javascript">
				  CKEDITOR.replace( '<?=$descricao;?>',
					{
                       toolbar :
						[
							[ 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ]
						],
						height : 200,
						disableObjectResizing : true,
						removePlugins : 'smiley',
						filebrowserBrowseUrl : '../ckeditor/filebrowser.php?action=browse',
						filebrowserUploadUrl : '../ckeditor/filebrowser.php?action=upload'
				  } );
				</script>
              </td>
		    </tr>
<?php
		if($upd != false){
			$id = $post[$codigo];
			echo '
			<tr>
			  <td></td>
			  <td id="Imagens'.$id.'">';
			$arquivos = explode("," , $post[$imagens]);
			for($i=0; $i<count($arquivos); $i++){
			    if(!$post[$imagens]){
				    echo "<span style=\"color: #ff0000;\">Nenhuma imagem cadastrada.</span>";
				}else{
				    $ext = ".".end(explode(".",$arquivos[$i]));
					
					$arquivo = current(explode(".",$arquivos[$i]));
					$arquivo = strtr($arquivo, "_", " ");
					echo '
				<div style="margin-top: 3px;">
				  <a href="arquivos/produtos/'.$arquivos[$i].'" rel="highslides" onclick="return hs.expand(this)">'.$arquivo.$ext.'</a> - 
				  <a href="javascript: $(\'#Imagens'.$id.'\').load(\'admin.php?menu=cad_pro&acao=del_item&codigo='.$id.'&item='.$i.'\');" class="menu">Remover</a>
				</div>';
				}
		    }
			echo '
			  </td>
			</tr>';
		}
?>
			<tr>
              <td valign="top" style="padding-top: 8px;"><label for="<?=$imagens;?>">Imagens</label></td>
              <td>
                <input type="file" name="<?=$imagens;?>[]" size="25" style="width: 315px;" />
                
                <p id="Campos"></p>
				
				<a href="javascript: void(0);" onClick="NovoItem('imagens', 'Campos');" class="menu">Anexar outra imagem</a>
              </td>
            </tr>
		  </table>
		  <br />
		  <br />
		  <p>* Campos destacados s&atilde;o obrigat&oacute;rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_pro" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" class="bts" onclick="envia_produto(this.form)" />
	      </center>
		</form>
        <?php
    }

//------------------------------------------------------------------------------------------------
    function lista_produtos($erro=false){
		$sql = mysql_query("SELECT P.*, C.titulo AS categoria FROM $this->tabela_produtos P, categorias C WHERE P.cod_categoria = C.codigo ORDER BY C.titulo, P.$this->produtos_titulo");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de produtos</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_pro&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Nova</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
        	      <td class="lista_tit cel_tabela" width="42%" align="left">Categoria</td>
				  <td class="lista_tit cel_tabela" width="42%" align="left">T&iacute;tulo</td>
        	      <td class="lista_tit cel_tabela" colspan="3" width="16%" align="center">
				    <p style="width: 150px;">A&ccedil;&otilde;es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			//formata data
			echo '
			    <tr>
                  <td class="cel_tabela" align="left">'.$reg['categoria'].'</td>
				  <td class="cel_tabela" align="left">'.$reg[$this->produtos_titulo].'</td>
                  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_pro&acao=edit&codigo='.$reg[$this->produtos_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_pro&acao=excl&codigo='.$reg[$this->produtos_codigo].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_produto($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_produtos WHERE $this->produtos_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_produto($codigo){
		$arq = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_produtos WHERE $this->produtos_codigo = ".$codigo));
		
		$arquivos = explode("," , $arq[$this->produtos_imagens]);
		
		for($i = 0; $i < count($arquivos); $i++){
			@unlink('arquivos/produtos/'.trim($arquivos[$i]));
			
			$ext = ".".end(explode(".", $arquivos[$i]));
			@unlink('arquivos/produtos/'.str_replace($ext, "_mini".$ext, trim($arquivos[$i])));
		}
		
		mysql_query("DELETE FROM $this->tabela_produtos WHERE $this->produtos_codigo = ".$codigo);
		
		$this->lista_produtos();
	}
	
//------------------------------------------------------------------------------------------------
	function exclui_item($id, $item){
		//consulta arquivos cadastrados
		$Consulta = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_produtos WHERE $this->produtos_codigo = ".$id));
		//gera array dos arquivos cadastrados
		$ArrayArquivos = explode(",", $Consulta[$this->produtos_imagens]);
		//deleta arquivo do disco
		@unlink("arquivos/produtos/".$ArrayArquivos[$item]);
		//Gera nome e remove do disco a img miniatura
		$ext = ".".end(explode(".", $ArrayArquivos[$item]));
		@unlink('arquivos/produtos/'.str_replace($ext, "_mini".$ext, trim($ArrayArquivos[$item])));
		
		//remove item do array
		unset($ArrayArquivos[$item]);
		//gera novo array de elementos
		$count = count($ArrayArquivos);
		for ($i = 0; $i <= $count; $i++) {
			if($ArrayArquivos[$i] != ""){
				//gera novo index pro array
				if(!$NovoIndexArray){ $NovoIndexArray = "1"; }
				if($NovoIndexArray == "1"){
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.$ArrayArquivos[$i];
				} else {
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.",".$ArrayArquivos[$i];
				}
			}
		}
		//imprime itens que nao foram removidos
		$arquivos = explode("," , $Arquivos);
		$NumeroArquivos = count($arquivos);
		if($arquivos[0] != ""){
			for($i=0;$i<$NumeroArquivos;$i++){
				$ext = ".".end(explode(".",$arquivos[$i]));
				$arquivo = current(explode(".",$arquivos[$i]));
				$arquivo = strtr($arquivo, "_", " ");
				echo '<div style="margin-top: 3px;">
						<a href="arquivos/produtos/'.$arquivos[$i].'" target="_blank">'.$arquivo.$ext.'</a> - 
						<a href="javascript: CarregaUrl(\'admin.php?menu=cad_pro&acao=del_item&codigo='.$id.'&item='.$i.'\', \'Imagens'.$id.'\');" class="menu">Remover</a>
					  </div>';
			}
		}else{
			echo '<span style="color: #ff0000;">Nenhuma imagem cadastrada.</span>';
		}
		//grava altera�oes no sql
		mysql_query("UPDATE $this->tabela_produtos SET $this->produtos_imagens='".$Arquivos."' WHERE $this->produtos_codigo = ".$id);
	}

//------------------------------------------------------------------------------------------------
	function insere_produto($post){
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->produtos_imagens], "arquivos/produtos/", true);
		
		if($arquivos == "invalido"){
			$this->imprime_form($post, false, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			exit();
			
		}else{
			mysql_query("
			  INSERT INTO $this->tabela_produtos (
				  $this->produtos_cod_categoria
				, $this->produtos_titulo
				, $this->produtos_descricao
				, $this->produtos_imagens
			  )VALUES (
				  '".$this->LimpaString($post[$this->produtos_cod_categoria])."'
				, '".$this->LimpaString($post[$this->produtos_titulo])."'
				, '".$post[$this->produtos_descricao]."'
				, '".$arquivos."'
			  )");
			
			if(mysql_error()){
				$array = explode("," , $arquivos);
		        
				for($i=0; $i<count($array); $i++){
					@unlink('arquivos/produtos/'.trim($array[$i]));
					
					$ext = ".".end(explode(".", $array[$i]));
					$arq_mini = str_replace($ext, "_mini".$ext, trim($array[$i]));
					@unlink('arquivos/produtos/'.trim($arq_mini));
				}
				
				$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.<br>".mysql_error());
				exit();
				
            }else{
				$this->lista_produtos();
				
			}
		}
    }

//------------------------------------------------------------------------------------------------
	function atualiza_produto($post){
		//seleciona banco
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_produtos 
		         WHERE codigo = ".$post[$this->produtos_codigo]));
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->produtos_imagens], "arquivos/produtos/", true);
		
		if($arquivos == "invalido"){
			$post[$this->produtos_imagens] = $reg[$this->produtos_imagens];
			$this->imprime_form($post, true, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			
			exit;
		}else{
			if($arquivos){
				if($reg[$this->produtos_imagens])
					$arquivos = str_replace(",,", ",", $reg[$this->produtos_imagens].",".$arquivos);
					
				$coluna = ", $this->produtos_imagens   = '".$arquivos."'";
			}
			
			mysql_query("
			  UPDATE $this->tabela_produtos SET
				  $this->produtos_cod_categoria= '".$this->LimpaString($post[$this->produtos_cod_categoria])."'
				, $this->produtos_titulo       = '".$this->LimpaString($post[$this->produtos_titulo])."'
				, $this->produtos_descricao    = '".$post[$this->produtos_descricao]."'
				$coluna
			  WHERE $this->produtos_codigo = ".$post[$this->produtos_codigo]);
			
			$this->lista_produtos();
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}

//------------------------------------------------------------------------------------------------
	function mostra_produtos($categoria=false){
		if(!$categoria){
			$categoria = mysql_fetch_array(mysql_query("SELECT codigo FROM categorias ORDER BY titulo ASC"));
			$categoria = $categoria['codigo'];
		}
		
        $sql = mysql_query("SELECT * FROM $this->tabela_produtos WHERE $this->produtos_cod_categoria = $categoria ORDER BY $this->produtos_titulo ASC");
		
		echo '<div class="row">';
		
		
		while($reg = mysql_fetch_array($sql)){
			$imagens = explode(',',$reg[$this->produtos_imagens]);
?>
						<div class="col-md-4">
                            <div class="post3">
                                <a href="produtos.php?id=<?php echo $reg[$this->produtos_codigo];?>">
                                	<img src="<?php echo URLADMIN .'/arquivos/produtos/'.$imagens[0];?>" alt="">
                                    <p><?php echo $reg[$this->produtos_titulo];?></p>
                                </a>
                            </div>
                        </div>
<?php			
        }
		
		if(mysql_num_rows($sql) == 0){
			echo '
				<p class="destaque" >Nenhum produto cadastrada.</p>';
		}
		
		echo '</div>';
    }
	
//------------------------------------------------------------------------------------------------	
	function detalhes_produto($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_produtos WHERE $this->produtos_codigo = ".$this->anti_sql_injection($codigo)));
		
		//busca a 1� imagem
		$arquivos = explode(",", $reg[$this->produtos_imagens]);
		
		echo '<p>'.$reg[$this->produtos_titulo].'</p>
			  <div class="ck">'.$reg[$this->produtos_descricao].'</div>';
		
		if($reg[$this->produtos_imagens]){
			$contador = 0;
			echo '
			    <div class="row">';
			for($i=0; $i<count($arquivos); $i++){
				if($contador >= 3){
					echo '
			    </div>
			    <div class="row">';
					$contador = 0;
				}
				$contador++;
				
				echo '
				<div class="col-md-4">
                  <div class="post3">
				    <a href="'.URLADMIN.'/arquivos/produtos/'.$arquivos[$i].'" rel="highslide" onclick="return hs.expand(this)">
					  <img src="'.URLADMIN.'/arquivos/produtos/'.str_replace(".jpg","_mini.jpg", $arquivos[$i]).'"  alt=""/>
				    </a>
				  </div>
				</div>';
			}
			echo '
			    </div>';
		}
	}
	
}

?>
