<?php
class noticias extends body{
    
    function __construct(){
        body :: __construct();
		noticias :: tabela_noticias();
    }

//------------------------------------------------------------------------------------------------
	function tabela_noticias(){
		$this->tabela_noticias       = 'noticias';
		$this->noticias_codigo       = 'codigo';
		$this->noticias_data_noticia = 'data_noticia';
		$this->noticias_titulo       = 'titulo';
		$this->noticias_descricao    = 'descricao';
		$this->noticias_imagens      = 'imagens';
		$this->noticias_video        = 'video';
	}

//------------------------------------------------------------------------------------------------
    function imprime_form($post=false, $upd=false, $erro=false){
		$codigo       = $this->noticias_codigo;
		$data_noticia = $this->noticias_data_noticia;
		$titulo       = $this->noticias_titulo;
		$descricao    = $this->noticias_descricao;
		$imagens      = $this->noticias_imagens;
		$video        = $this->noticias_video;

		if($upd == true){
			$nome_tela   = "Alterar noticia";
			$nome_botao  = 'atualizar';
			$valor_botao = 'Editar';
			$action      = 'admin.php?menu=cad_not&codigo='.$_GET['codigo']."&atualizar=true";
		}else{
			$nome_tela   = "Nova noticia";
			$nome_botao  = 'inserir';
			$valor_botao = 'Gravar';
			$action      = 'admin.php?menu=cad_not&inserir=true';
		}
		
		if($post[$data_noticia]){
			//formata data
			$array = explode("-", $post[$data_noticia]);
			$post[$data_noticia] = $array[2]."/".$array[1]."/".$array[0];
		}
		
		echo '
		<p class="menu_item" style="margin: 0px 0px 15px 0px; text-align: center; background:#e4e4e4;">
          '.$nome_tela.'
        </p>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
		echo '
        <form name="form" action="'.$action.'" method="post" enctype="multipart/form-data" style="width: 500px; text-align: left; margin: auto;">';
		
		if($upd == true)
			echo '<input type="hidden" id="'.$codigo.'" name="'.$codigo.'" value="'.$post[$codigo].'" />';
?>
          <table width="100%" cellpadding="3" cellspacing="3" border="0">
		    <tr>
              <td width="15%"><label for="<?=$data_noticia;?>">Data *</label></td>
			  <td width="85%"><input name="<?=$data_noticia;?>" type="text" id="<?=$data_noticia;?>" value="<?=$post[$data_noticia];?>" maxlength="10" onkeypress="MascaraFormata(this, '##/##/####'); return sonum(event);" class="toolTip2" tooltip2="Informe uma data" /> exe: dd/mm/aaaa</td>
		    </tr>
            <tr>
              <td><label for="<?=$titulo;?>">Titulo *</label></td>
			  <td><input name="<?=$titulo;?>" type="text" id="<?=$titulo;?>" value="<?=$post[$titulo];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe um titulo"/></td>
		    </tr>
		    <tr>
			  <td height="20"><label for="<?=$descricao;?>" style="margin-bottom: 2px;">Descri&ccedil;&atilde;o</label></td>
            </tr>
            <tr>
              <td colspan="2">
                <textarea name="<?=$descricao;?>" style="width: 100%; height: 200px;"><?=$post[$descricao];?></textarea>
                <script type="text/javascript">
				  CKEDITOR.replace( '<?=$descricao;?>',
					{
                       toolbar :
						[
							[ 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ]
						],
						height : 200,
						disableObjectResizing : true,
						removePlugins : 'smiley',
						filebrowserBrowseUrl : '../ckeditor/filebrowser.php?action=browse',
						filebrowserUploadUrl : '../ckeditor/filebrowser.php?action=upload'
				  } );
				</script>
              </td>
		    </tr>
		    <tr>
              <td><label for="<?=$video;?>">V&iacute;deo *</label></td>
			  <td><input name="<?=$video;?>" type="text" id="<?=$video;?>" value="<?=$post[$video];?>" size="42" style="width: 315px;"  class="toolTip2" tooltip2="Informe o c�digo do v�deo"/></td>
		    </tr>
		    
<?php
		if($upd != false){
			$id = $post[$codigo];
			echo '
			<tr>
			  <td></td>
			  <td id="Imagens'.$id.'">';
			$arquivos = explode("," , $post[$imagens]);
			for($i=0; $i<count($arquivos); $i++){
			    if(!$post[$imagens]){
				    echo "<span style=\"color: #ff0000;\">Nenhuma imagem cadastrada.</span>";
				}else{
					$ext = explode(".",$arquivos[$i]);
					$qt = count($ext);
					$ext = ".".$ext[$qt-1];
					$arquivo = current(explode(".",$arquivos[$i]));
					
					echo '
				<div style="margin-top: 3px;">
				  <a href="arquivos/noticias/'.$arquivos[$i].'" rel="highslides" onclick="return hs.expand(this)">'.$arquivo.$ext.'</a> - 
				  <a href="javascript: $(\'#Imagens'.$id.'\').load(\'admin.php?menu=cad_not&acao=del_item&codigo='.$id.'&item='.$i.'\');" class="menu">Remover</a>
				</div>';
				}
		    }
			echo '
			  </td>
			</tr>';
		}
?>
			<tr>
              <td valign="top" style="padding-top: 8px;"><label for="<?=$imagens;?>">Imagens</label></td>
              <td>
                <input type="file" name="<?=$imagens;?>[]" size="25" style="width: 315px;" />
                
                <p id="Campos"></p>
				
				<a href="javascript: void(0);" onClick="NovoItem('imagens', 'Campos');" class="menu">Anexar outra imagem</a>
              </td>
            </tr>
		  </table>
		  <br />
		  <br />
		  <p>* Campos destacados s&atilde;o obrigat&oacute;rios.</p>
          <br />
          <center>
		    <a href="admin.php?menu=cad_not" class="menu">&laquo; voltar</a>
		    |
		    <input type="button" id="<?=$nome_botao;?>" name="<?=$nome_botao;?>" value="<?=$valor_botao;?>" class="bts" onclick="envia_noticia(this.form)" />
	      </center>
		</form>
        <?php
    }

//------------------------------------------------------------------------------------------------
    function lista_noticias($erro=false){
		$sql = mysql_query("SELECT * FROM $this->tabela_noticias ORDER BY $this->noticias_data_noticia DESC");
        
		echo '<table width="100%" align="center" cellpadding="0" cellspacing="0">
		        <tr>
				  <td class="menu_item" align="center" style="background:#e4e4e4;">&nbsp;Cadastro de noticias</td>
				</tr>
				<tr>
				  <td>
				    <a href="admin.php?menu=cad_not&acao=novo" class="menu"><img src="../imagens/add.gif" align="absmiddle"> &laquo; Nova</a>
				  </td>
				</tr>
		      </table>';
		
		if($erro)
			echo '<p class="exibe_erro">'.$erro.'</p>';
		
        echo '<table border="0" align="center" width="100%">
			    <tr style="background: #ededed">
        	      <td class="lista_tit cel_tabela" width="10%" align="center">Data</td>
        	      <td class="lista_tit cel_tabela" width="74%" align="left">T&iacute;tulo</td>
        	      <td class="lista_tit cel_tabela" colspan="3" width="16%" align="center">
				    <p style="width: 150px;">A&ccedil;&otilde;es</p>
				  </td>
        	    </tr>';

        while($reg = mysql_fetch_array($sql)){
			//formata data
			$array = explode("-", $reg[$this->noticias_data_noticia]);
			$reg[$this->noticias_data_noticia] = $array[2]."/".$array[1]."/".$array[0];
			echo '
			    <tr>
                  <td class="cel_tabela" align="center">'.$reg[$this->noticias_data_noticia].'</td>
                  <td class="cel_tabela" align="left">'.$reg[$this->noticias_titulo].'</td>
                  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_not&acao=edit&codigo='.$reg[$this->noticias_codigo].'" class="menu">Editar</a>
                  </td>
				  <td class="cel_tabela" align="center" width="8%">
                    <a href="admin.php?menu=cad_not&acao=excl&codigo='.$reg[$this->noticias_codigo].'" onclick="return confirm(\'Tem certeza que deseja excluir este item?\');" class="menu">Excluir</a>
                  </td>
                </tr>';
        }
		echo '</table>';
    }

//------------------------------------------------------------------------------------------------
	function get_noticia($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_noticias WHERE $this->noticias_codigo = ".$codigo));
		
		return $reg;
	}

//------------------------------------------------------------------------------------------------
	function exclui_noticia($codigo){
		$arq = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_noticias WHERE $this->noticias_codigo = ".$codigo));
		
		$arquivos = explode("," , $arq[$this->noticias_imagens]);
		
		for($i = 0; $i < count($arquivos); $i++){
			@unlink('arquivos/noticias/'.trim($arquivos[$i]));
			
			$ext = ".".end(explode(".", $arquivos[$i]));
			@unlink('arquivos/noticias/'.str_replace($ext, "_mini".$ext, trim($arquivos[$i])));
		}
		
		mysql_query("DELETE FROM $this->tabela_noticias WHERE $this->noticias_codigo = ".$codigo);
		
		$this->lista_noticias();
	}
	
//------------------------------------------------------------------------------------------------
	function exclui_item($id, $item){
		//consulta arquivos cadastrados
		$Consulta = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_noticias WHERE $this->noticias_codigo = ".$id));
		//gera array dos arquivos cadastrados
		$ArrayArquivos = explode(",", $Consulta[$this->noticias_imagens]);
		//deleta arquivo do disco
		@unlink("arquivos/noticias/".$ArrayArquivos[$item]);
		//Gera nome e remove do disco a img miniatura
		
		$ext = explode(".",$ArrayArquivos[$item]);
		$qt = count($ext);
		$ext = ".".$ext[$qt-1];
				
		@unlink('arquivos/noticias/'.str_replace($ext, "_mini".$ext, trim($ArrayArquivos[$item])));
		
		//remove item do array
		unset($ArrayArquivos[$item]);
		//gera novo array de elementos
		$count = count($ArrayArquivos);
		for ($i = 0; $i <= $count; $i++) {
			if($ArrayArquivos[$i] != ""){
				//gera novo index pro array
				if(!$NovoIndexArray){ $NovoIndexArray = "1"; }
				if($NovoIndexArray == "1"){
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.$ArrayArquivos[$i];
				} else {
					$NovoIndexArray = $NovoIndexArray+1;
					$Arquivos = $Arquivos.",".$ArrayArquivos[$i];
				}
			}
		}
		//imprime itens que nao foram removidos
		$arquivos = explode("," , $Arquivos);
		$NumeroArquivos = count($arquivos);
		if($arquivos[0] != ""){
			for($i=0;$i<$NumeroArquivos;$i++){
				$ext = explode(".",$$arquivos[$i]);
				$qt = count($ext);
				$ext = ".".$ext[$qt-1];
				
				$arquivo = current(explode(".",$arquivos[$i]));
				$arquivo = strtr($arquivo, "_", " ");
				echo '<div style="margin-top: 3px;">
						<a href="arquivos/noticias/'.$arquivos[$i].'" target="_blank">'.$arquivo.$ext.'</a> - 
						<a href="javascript: CarregaUrl(\'admin.php?menu=cad_not&acao=del_item&codigo='.$id.'&item='.$i.'\', \'Imagens'.$id.'\');" class="menu">Remover</a>
					  </div>';
			}
		}else{
			echo '<span style="color: #ff0000;">Nenhuma imagem cadastrada.</span>';
		}
		//grava altera�oes no sql
		mysql_query("UPDATE $this->tabela_noticias SET $this->noticias_imagens='".$Arquivos."' WHERE $this->noticias_codigo = ".$id);
	}

//------------------------------------------------------------------------------------------------
	function insere_noticia($post){
		//formata data
		$array = explode("/", $post[$this->noticias_data_noticia]);
		$post[$this->noticias_data_noticia] = $array[2]."-".$array[1]."-".$array[0];
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->noticias_imagens], "arquivos/noticias/", true);
		
		if($arquivos == "invalido"){
			$this->imprime_form($post, false, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			exit();
			
		}else{
			mysql_query("
			  INSERT INTO $this->tabela_noticias (
				  $this->noticias_data_noticia
				, $this->noticias_titulo
				, $this->noticias_descricao
				, $this->noticias_imagens
				, $this->noticias_video
			  )VALUES (
				  '".$post[$this->noticias_data_noticia]."'
				, '".$this->LimpaString($post[$this->noticias_titulo])."'
				, '".$post[$this->noticias_descricao]."'
				, '".$arquivos."'
				, '".$post[$this->noticias_video]."'
			  )");
			
			if(mysql_error()){
				$array = explode("," , $arquivos);
		        
				for($i=0; $i<count($array); $i++){
					@unlink('arquivos/noticias/'.trim($array[$i]));
					
					$ext = ".".end(explode(".", $array[$i]));
					$arq_mini = str_replace($ext, "_mini".$ext, trim($array[$i]));
					@unlink('arquivos/noticias/'.trim($arq_mini));
				}
				
				$this->imprime_form($post, false, "Ocorreu um erro ao inserir o registro. Por favor verifique.<br>".mysql_error());
				exit();
				
            }else{
				$this->lista_noticias();
				
			}
		}
    }

//------------------------------------------------------------------------------------------------
	function atualiza_noticia($post){
		//formata data
		$array = explode("/", $post[$this->noticias_data_noticia]);
		$post[$this->noticias_data_noticia] = $array[2]."-".$array[1]."-".$array[0];
		
		//seleciona banco
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_noticias 
		         WHERE codigo = ".$post[$this->noticias_codigo]));
		
		require "upload/upload.php";
		
		$arquivos = uploadFotos("600", $_FILES[$this->noticias_imagens], "arquivos/noticias/", true);
		
		if($arquivos == "invalido"){
			$post[$this->noticias_imagens] = $reg[$this->noticias_imagens];
			$this->imprime_form($post, true, "Imagens em formato inv�lido. Os arquivos devem ser no formato JPG ou JPEG.");
			
			exit;
		}else{
			if($arquivos){
				if($reg[$this->noticias_imagens])
					$arquivos = str_replace(",,", ",", $reg[$this->noticias_imagens].",".$arquivos);
					
				$coluna = ", $this->noticias_imagens   = '".$arquivos."'";
			}
			
			mysql_query("
			  UPDATE $this->tabela_noticias SET
				  $this->noticias_data_noticia = '".$post[$this->noticias_data_noticia]."'
				, $this->noticias_titulo       = '".$this->LimpaString($post[$this->noticias_titulo])."'
				, $this->noticias_descricao    = '".$post[$this->noticias_descricao]."'
				, $this->noticias_video        = '".$post[$this->noticias_video]."'
				$coluna
			  WHERE $this->noticias_codigo = ".$post[$this->noticias_codigo]);
			
			$this->lista_noticias();
		}
	}
	
//------------------------------------------------------------------------------------------------	
	function LimpaString($str){
		return addslashes(htmlspecialchars($str));
		
	}


//------------------------------------------------------------------------------------------------	
	function mostra_home(){
		$sql = mysql_query("SELECT * FROM $this->tabela_noticias ORDER BY data_noticia DESC LIMIT 3");
?>
          
                	<div class="row">
<?php
		$qtd = mysql_num_rows($sql);
		while($reg = mysql_fetch_array($sql)){
			
			$data = explode("-",$reg[$this->noticias_data_noticia]);
			$imagens = explode(',',$reg[$this->noticias_imagens]);
?>
                        <div class="col-md-<?php echo ($qtd == 3 ? 4 : ($qtd == 2 ? 6 : 12));?>">
                            <div class="post3">
                                <a href="noticias.php?id=<?php echo $reg[$this->noticias_codigo];?>">
                                	<img src="<?php echo URLADMIN .'/arquivos/noticias/'.$imagens[0];?>" alt="">
                                
                                    <time datetime="<?php echo $reg[$this->noticias_data_noticia];?>">
                                        <span class="year"><?php echo $data[0];?></span>
                                        <span class="month"><?php echo $this->mes_extenso(number_format($data[1]));?></span>
                                    </time>
                                    <p><?php echo $reg[$this->noticias_titulo];?></p>
                                </a>
                            </div>
                        </div>
<?php
		}
?>
                    </div>
<?php
	}

//------------------------------------------------------------------------------------------------
	function mostra_noticias($codigo=false){
        $sql = mysql_query("SELECT * FROM $this->tabela_noticias ORDER BY $this->noticias_data_noticia DESC");
		
		if(!$codigo){
			$reg_cod = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_noticias ORDER BY $this->noticias_data_noticia DESC"));
			$codigo = $reg_cod[$this->noticias_codigo];
		}
		
		if($codigo)
			$this->detalhes_noticia($codigo);
		
		
		while($reg = mysql_fetch_array($sql)){
			//formata data
			$array = explode("-", $reg[$this->noticias_data_noticia]);
			$reg[$this->noticias_data_noticia] = $array[2]."/".$array[1]."/".$array[0];
			
			echo '<p><a href="noticias.php?id='.$reg[$this->noticias_codigo].'">'.$reg[$this->noticias_data_noticia].' - '.$reg[$this->noticias_titulo].'</a></p>';
        }
		
		if(mysql_num_rows($sql) == 0){
			echo '
				<p class="destaque center" >Nenhuma noticia cadastrada.</p>';
		}
		
		
    }
	
//------------------------------------------------------------------------------------------------	
	function detalhes_noticia($codigo){
		$reg = mysql_fetch_array(mysql_query("SELECT * FROM $this->tabela_noticias WHERE $this->noticias_codigo = ".$this->anti_sql_injection($codigo)));
		
		//formata data
		$array = explode("-", $reg[$this->noticias_data_noticia]);
		$reg[$this->noticias_data_noticia] = $array[2]."/".$array[1]."/".$array[0];
		
		//busca a 1� imagem
		$arquivos = explode(",", $reg[$this->noticias_imagens]);
		
		echo '<p>'.$reg[$this->noticias_data_noticia].'</p>
			  <p>'.$reg[$this->noticias_titulo].'</p>
			  <div class="ck">'.$reg[$this->noticias_descricao].'</div>';
		if($reg[$this->noticias_video] != '')
			echo '	  
			  <div>
			  	<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$reg[$this->noticias_video].'?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			  </div>';
		
		if($reg[$this->noticias_imagens]){
			$contador = 0;
			echo '
			    <div class="row">';
			for($i=0; $i<count($arquivos); $i++){
				if($contador >= 3){
					echo '
			    </div>
			    <div class="row">';
					$contador = 0;
				}
				$contador++;
				
				echo '
				<div class="col-md-4">
                  <div class="post3">
				    <a href="'.URLADMIN.'/arquivos/noticias/'.$arquivos[$i].'" rel="highslide" onclick="return hs.expand(this)">
					  <img src="'.URLADMIN.'/arquivos/noticias/'.str_replace(".jpg","_mini.jpg", $arquivos[$i]).'"  alt=""/>
				    </a>
				  </div>
				</div>';
			}
			echo '
			    </div>';
		}
	}
	
}

?>
