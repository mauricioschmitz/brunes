<?php
require 'path.php';

require 'include_share/class.noticias.php';

$PAGE = new body();
$PAGE_NOTICIAS = new noticias();

$PAGE->conecta_banco();

//parametros para otimização do google
$parametros['title'] = TITULO;
$parametros['keywords'] = "";
$parametros['description'] = "";
$PAGE->imprime_cabecalho_site($parametros, URLSITE);
?>

<div class="container-fluid">  
    <?=$PAGE->imprime_topo('home');?>
    <?php //$PAGE_BANNERS->mostra_banners();?> 
    <section id="conteudos">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            	<h1 style="color:#000;">NOT&Iacute;CIAS</h1>
<?php 
	$PAGE_NOTICIAS->mostra_noticias($_GET['id']);
?>
            </div>	 
		</div>            
	</section> 
    
    <?php echo $PAGE->rodape_site();?>
</div>

<?php
//$PAGE->imprime_topo('home');
/*
?>
  <div id="banners"><?=$PAGE_BANNERS->mostra_banners();?></div>
  <div class="clear"></div>
  <div style="height:450px;"></div>
<? $PAGE->imprime_rodape(true); */?>