// JavaScript Document
var email_padrao = 'email@servidor.com';
var senha_padrao = 'Digite sua senha';

$('a.link').live('click', function() { //When clicking on the close or fade layer...
	url = this.rel;
	$('#conteudo').load(url);
	return false;
});

function envia_contatos(form){
	erro = '';
	if(form.nome.value == ''){
		erro = 'O campo nome é obrigatório';
		form.nome.focus;	
	}else if(form.email.value == '' || form.email.value == email_padrao){
		erro = 'O campo email é obrigatório';	
		form.email.focus;
	}else if(form.assunto.value == ''){
		erro = 'O campo assunto é obrigatório';
		form.assunto.focus;	
	}else if(form.mensagem.value == ''){
		erro = 'O campo mensagem é obrigatório';
		form.mensagem.focus;	
	}
	
	if(erro != ''){
		$('#resposta').load('contato.php?menu=erro&erro='+escape(erro));
	}else{
		nome = form.nome.value;
		email = form.email.value;
		telefone  = form.telefone.value;
		assunto = form.assunto.value;
		mensagem = form.mensagem.value;
		$.ajax({
		  type: "POST",
		  url: 'contato.php?menu=ok&acao=enviar_contato',
		  data: 'nome='+escape(nome)+'&email='+escape(email)+'&telefone='+escape(telefone)+'&assunto='+escape(assunto)+'&mensagem='+escape(mensagem),
		  success: function(response){
			  response = response.split('|');
			  if(response[1] == 'ok'){
			  	  $('#resposta').load('contato.php?menu=retorno&retorno='+escape(response[0]));
				  form.nome.value = '';
				  form.email.value = '';
				  form.telefone.value = '';
				  form.assunto.value = '';
				  form.mensagem.value = '';
			  }else
			      $('#resposta').load('contato.php?menu=erro&erro='+escape(response[0]));
			  
		  },
		  erro: function(){
			  alert('Ocorreu um erro ao enviar a requisição');
		  },
		  dataType: 'html'
		});	
	}
}

function envia_recado(form){
	erro = '';
	if(form.nome.value == ''){
		erro = 'O campo nome é obrigatório';
		form.nome.focus;	
	}else if(form.email.value == '' || form.email.value == email_padrao){
		erro = 'O campo email é obrigatório';	
		form.email.focus;
	}else if(form.recado.value == ''){
		erro = 'O campo recado é obrigatório';
		form.recado.focus;	
	}
	
	if(erro != ''){
		$('#resposta').load('mural-de-recados.php?menu=erro&erro='+escape(erro));
	}else{
		nome = form.nome.value;
		email = form.email.value;
		recado  = form.recado.value;
		$.ajax({
		  type: "POST",
		  url: 'mural-de-recados.php?menu=ok&acao=enviar_recado',
		  data: 'nome='+escape(nome)+'&email='+escape(email)+'&recado='+escape(recado),
		  success: function(response){
			  response = response.split('|');
			  if(response[1] == 'ok'){
			  	  $('#resposta').load('mural-de-recados.php?menu=retorno&retorno='+escape(response[0]));
				  form.nome.value = '';
				  form.email.value = '';
				  form.recado.value = '';
			  }else
			      $('#resposta').load('mural-de-recados.php?menu=erro&erro='+escape(response[0]));
			  
		  },
		  erro: function(){
			  alert('Ocorreu um erro ao enviar a requisição');
		  },
		  dataType: 'html'
		});	
	}
}

function MascaraFormata(src, mascara) {
	var campo   = src.value.length;
	var saida   = mascara.substring(0,1);
	var texto   = mascara.substring(campo);
	if(texto.substring(0,1) != saida) {
		src.value += texto.substring(0,1);
	}
}

function mostra_erro(apaga){
	$('.erro').fadeIn(1500,function(){
		if(!apaga){
			tempo = setTimeout("some_erro()",5000);
		}
	});
}

function some_erro(){
	clearTimeout(tempo);
	$('.erro').fadeOut(1500);
}

var tempo2;
function mostra_retorno(){
	$('.retorno').fadeIn(1500,function(){
		tempo2 = setTimeout("some_retorno()",5000);
	});
}

function some_retorno(){
	clearTimeout(tempo2);
	$('.retorno').fadeOut(1500);
}