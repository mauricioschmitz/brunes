var http_request = false;

function CriaAjax(){
	http_request = null;
	if (window.XMLHttpRequest) { 
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) { 
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
	if (!http_request) {
		alert('Sem suporte a Ajax!');
		return false;
	}
}

function CarregaUrl(url,div){
	CriaAjax();
	DirDiv = document.getElementById(div);
	DivTransparente = document;
	http_request.onreadystatechange = CarregaConteudo
	http_request.open('GET', url, true);
	http_request.send(null);
}

function CarregaConteudo() {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			DirDiv.innerHTML = http_request.responseText;
		} else {
			DirDiv.innerHTML = http_request.responseText;
			mostra_msg('Ocorreu um erro!', '');
	        MM_showHideLayers('transparente','','show','exemplo','','show');
		}
	} else {
		DirDiv.innerHTML = "<div style=\"text-align: center; color: #000;\">Carregando...<br /><img src=\"../imagens/Carregando.gif\" alt=\"Carregando\" /></div>";
	}
}

//cria os option nos campos selects
function CarregaOptions(url, check){
	CriaAjax();
	Check = check;
	http_request.onreadystatechange = CarregaConteudoOption
	http_request.open('GET', url, true);
	http_request.send(null);
}

function CarregaConteudoOption() {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			document.getElementById(Check).innerHTML = "";

			vetor = http_request.responseText;
			var array = vetor.split(",");
			
			for(i = 0; i < array.length; i++){
				if(array[i]){
					var array_val = array[i].split(".");
					cria_option(array_val['0'], array_val['1'], array_val['2'], Check);
				}
			}
		}
	}
}

function cria_option(valor, opcao, selected, check){
	var option = document.createElement('option');
	var text  = document.createTextNode(opcao);
	option.setAttribute("value", valor);
	if(selected == "selected"){
		option.setAttribute("selected", selected);
	}
	option.appendChild(text);
	
	document.getElementById(check).appendChild(option);	
}
