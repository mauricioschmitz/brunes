AutoId = 0;

function abre_popup(div){
	$('.mens').hide();
	document.getElementById(div).style.display = 'block';
}

function valida_campos(form){
    for (i=0;i<form.length;i++){
		if (form[i].value == ""){
			if (form[i].id.length > 0){
				var nome = form[i].name.substring(1,form[i].name.length);
				var ident = form[i].id;
				alert("O campo " + ident.replace('_', ' ') + " � obrigat�rio.");
				form[i].focus();
				return false;
			}
        }
    }
    return true;
}

function mostra_recado(id,acao){
	if(acao == 'abre'){
		$('.recado_'+id).fadeIn(100);	
	}else if(acao == 'fecha'){
		$('.recado_'+id).fadeOut(100);	
	}	
}

function mostra_erro(apaga){
	$('.erro').fadeIn(1500,function(){
		if(!apaga){
			tempo = setTimeout("some_erro()",5000);
		}
	});
}

function some_erro(){
	clearTimeout(tempo);
	$('.erro').fadeOut(1500);
}

var tempo2;
function mostra_retorno(){
	$('.retorno').fadeIn(1500,function(){
		tempo2 = setTimeout("some_retorno()",5000);
	});
}

function some_retorno(){
	clearTimeout(tempo2);
	$('.retorno').fadeOut(1500);
}

var email_padrao = 'email@servidor.com';
var senha_padrao = 'Digite sua senha';
var login_padrao = 'Digite seu login';


function envia_config(form){
	if(form.nome.value == ''){
		form.nome.focus();
		return false;
		
	}else if(form.email.value == '' || form.email.value == email_padrao){
		form.email.focus();
		return false;
		
	}else if(form.login.value == '' || form.login.value == login_padrao){
		form.login.focus();
		return false;
		
	}else{
		form.submit();
	}
}

function envia_login(form){
	if(form.login.value == '' || form.login.value == login_padrao){
		form.login.focus();
		return false;
		
	}else if(form.senha.value == '' || form.senha.value == senha_padrao){
		form.senha.focus();
		return false;
		
	}else{
		form.submit();
	}
}

function envia_banner(form){
	if(form.titulo.value == ''){
		form.titulo.focus();
		return false;
		
	}else{
		form.submit();
	}
}

function envia_cliente(form){
	if(form.titulo.value == ''){
		form.titulo.focus();
		return false;
		
	}else if(form.uf.value == ''){
		form.titulo.focus();
		return false;
		
	}else if(form.cod_municipio.value == ''){
		form.titulo.focus();
		return false;
		
	}else{
		form.submit();
	}
}

function envia_receita(form){
	if(form.titulo.value == ''){
		form.titulo.focus();
		return false;
		
	}else{
		form.submit();
	}
}

function envia_noticia(form){
	if(form.data_noticia.value == ''){
		form.data_noticia.focus();
		return false;
		
	}else if(form.titulo.value == ''){
		form.titulo.focus();
		return false;
		
	}else{
		form.submit();
	}
}

function envia_categoria(form){
	if(form.titulo.value == ''){
		form.titulo.focus();
		return false;
		
	}else{
		form.submit();
	}
}


function envia_produto(form){
	if(form.titulo.value == ''){
		form.titulo.focus();
		return false;
		
	}else{
		form.submit();
	}
}




function sonum(e) {
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		key = e.keyCode;
	} else if(e.which) {
		// netscape
		key = e.which;
	} else {
		// no event, so pass through
		return true;
	}
	if ((key > 47 && key < 58) || (key == 8 || key == 9 || key == 127)){
	}else{
		return false;
	}
}

function tecla(evt) {
    var key_code = evt.keyCode  ? evt.keyCode  :
                       evt.charCode ? evt.charCode :
                       evt.which    ? evt.which    : void 0;
 
 
    if (key_code == 13)
    {
        document.getElementById('bt_enviar').click();
    }
}

function MascaraFormata(src, mascara) {
	var campo   = src.value.length;
	var saida   = mascara.substring(0,1);
	var texto   = mascara.substring(campo);
	if(texto.substring(0,1) != saida) {
		src.value += texto.substring(0,1);
	}
}

function NovoItem(){
	AutoId = AutoId + 1;
	
	var div = document.createElement('div');
	div.setAttribute('id',AutoId);
	div.setAttribute('style','margin-top: 2px;');
	document.getElementById('Campos').appendChild(div);
	
	var input = document.createElement('input');
    input.setAttribute('type','file');
    input.setAttribute('name','imagens[]');
	input.setAttribute('size','25');
	input.setAttribute('style','width: 330px;');
	document.getElementById(AutoId).appendChild(input);
	
	var link = document.createElement('a');
	link.setAttribute('title','Remover item');
	link.setAttribute('id', 'remover');
	link.setAttribute('class','menu');
	link.setAttribute('href','javascript: RemoveItem('+AutoId+');');
	
	var texto = document.createTextNode(' Remover');
	link.appendChild(texto);
    document.getElementById(AutoId).appendChild(link);
}

function RemoveItem(id){
	document.getElementById('Campos').removeChild(document.getElementById(id));
}