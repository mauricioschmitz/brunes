<?php
require "../path.php";
require '../include_share/class.textos.php'; 
require '../include_share/class.acessos.php'; 
require '../include_share/class.banners.php';
require '../include_share/class.noticias.php';
require '../include_share/class.categorias_receitas.php';
require '../include_share/class.receitas.php';
require '../include_share/class.categorias.php';
require '../include_share/class.produtos.php';
require '../include_share/class.clientes.php';

$PAGE = new body();
$PAGE_TEXTOS = new textos();
$PAGE_ACESSOS = new acessos();
$PAGE_BANNERS = new banners();
$PAGE_NOTICIAS = new noticias();
$PAGE_CATEGORIAS_RECEITAS = new categorias_receitas();
$PAGE_RECEITAS = new receitas();
$PAGE_CATEGORIAS = new categorias();
$PAGE_PRODUTOS = new produtos();
$PAGE_CLIENTES = new clientes();

$PAGE->conecta_banco();
  
  

$PAGE->verifica_acesso();
if($_GET['menu'] == "cad_not" && $_GET['acao'] == "del_item" && is_numeric($_GET['codigo'])){
	$PAGE_NOTICIAS->exclui_item($_GET['codigo'], $_GET['item']);
	exit();
	
}elseif($_GET['menu'] == "cad_rec" && $_GET['acao'] == "del_item" && is_numeric($_GET['codigo'])){
	$PAGE_RECEITAS->exclui_item($_GET['codigo'], $_GET['item']);
	exit();
	
}elseif($_GET['menu'] == "cad_cat" && $_GET['acao'] == "del_item" && is_numeric($_GET['codigo'])){
	$PAGE_CATEGORIAS->exclui_item($_GET['codigo'], $_GET['item']);
	exit();
	
}elseif($_GET['menu'] == "cad_cat_rec" && $_GET['acao'] == "del_item" && is_numeric($_GET['codigo'])){
	$PAGE_CATEGORIAS_RECEITAS->exclui_item($_GET['codigo'], $_GET['item']);
	exit();
	
}elseif($_GET['menu'] == "cad_pro" && $_GET['acao'] == "del_item" && is_numeric($_GET['codigo'])){
	$PAGE_PRODUTOS->exclui_item($_GET['codigo'], $_GET['item']);
	exit();
	
}elseif($_GET['menu'] == "cad_cli" && $_GET['acao'] == "set_uf" && $_GET['uf']){
	$sql = mysql_query("SELECT codigo, nome FROM municipio WHERE uf = '".$_GET['uf']."' ORDER BY nome");
	while($reg = mysql_fetch_array($sql)){
		echo '<option value="'.$reg['codigo'].'">'.utf8_encode($reg['nome']).'</option>';
	}
	exit();
	
}
//Final da execução ajax.
$parametros['title'] = TITULO;
$parametros['keywords'] = "";
$parametros['description'] = "";
$PAGE_TEXTOS->imprime_cabecalho($parametros, URLADMIN);

?>
<body>
  <div id="topo">
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td width="200" valign="top"><img src="../imagens/logo_admin.png" alt="<?=TITULO;?>" title="<?=TITULO;?>" /></td>
        <td width="23" valign="top">
<?
if(!$_GET['menu']){
	echo '<img src="../imagens/menu_home_ativo.jpg" alt="" title="" />';
	$menu = 'home';
}else{
	echo '<img src="../imagens/menu_home_inativo.jpg" alt="" title="" />';
	$menu = $_GET['menu'];
}
?>        
        </td>
        <td valign="top">
          <div id="bem_vindo">
            <div style="float:left">
              <?=$PAGE_TEXTOS->mostra_data();?> | Seja Bem Vindo: <b><?=$_SESSION['USUARIO_NOME'];?></b>
            </div>
            <div style="float:right; margin-right:10px;" class="bold">V. <?=VERSAO;?></div>
          </div>
          <div id="logout" style="z-index:10"><a href="logout.php?codigo=<?=$_SESSION['ATENDENTE_ID'];?>"><img src="../imagens/logout.png" alt="Sair" title="Sair"></a></div>
          <div id="faixa_menu">
            
          </div>
          <div id="menu">
              <?=$PAGE_TEXTOS->menu($menu);?>
             
            </div>
          <div id="livre"></div>
        </td>
      </tr>
    </table>
  </div>
  
<?

  if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'alt_tex'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">TEXTOS</span>
  </div>';
  	  	
		if($_POST){
			$PAGE_TEXTOS->atualizar_texto($_POST);
		}else{
			$reg = mysql_fetch_array(mysql_query("SELECT * FROM textos WHERE tela = '".$_GET['tela']."'"));
			$PAGE_TEXTOS->imprime_editor($reg);	
		}
		
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_config'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">CONFIGURA&Ccedil;&Otilde;ES</span>
  </div>';
  	  	
		if($_GET['acao'] == 'config' && $_GET['atualizar']){
			$PAGE_ACESSOS->atualiza_acesso($_POST);
				
		}else{
			$reg = mysql_fetch_array(mysql_query("SELECT * FROM usuarios WHERE codigo = ".$_SESSION['USUARIO_ID']));
			$PAGE_ACESSOS->form_config($reg,true);
		}
		
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_ban'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">BANNERS</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_BANNERS->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_BANNERS->insere_banner($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_ban = $PAGE_BANNERS->get_banner($_GET['codigo']);
			$PAGE_BANNERS->imprime_form($reg_ban, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_BANNERS->atualiza_banner($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_BANNERS->exclui_banner($_GET['codigo'],$_GET['arquivo']);
			
        }elseif($_GET['acao'] == 'recorta_imagem' && is_numeric($_GET['codigo'])){
	  		$PAGE_BANNERS->recorta_imagem($_GET['codigo'],true);
			
        }else{
			$PAGE_BANNERS->lista_banners();
			
        }
			
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_not'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">NOT&Iacute;CIAS</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_NOTICIAS->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_NOTICIAS->insere_noticia($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_nov = $PAGE_NOTICIAS->get_noticia($_GET['codigo']);
			$PAGE_NOTICIAS->imprime_form($reg_nov, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_NOTICIAS->atualiza_noticia($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_NOTICIAS->exclui_noticia($_GET['codigo']);
			
        }else{
			$PAGE_NOTICIAS->lista_noticias();
			
        }
			
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_cat_rec'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">CATEGORIAS RECEITAS</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_CATEGORIAS_RECEITAS->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_CATEGORIAS_RECEITAS->insere_categoria($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_nov = $PAGE_CATEGORIAS_RECEITAS->get_categoria($_GET['codigo']);
			$PAGE_CATEGORIAS_RECEITAS->imprime_form($reg_nov, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_CATEGORIAS_RECEITAS->atualiza_categoria($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_CATEGORIAS_RECEITAS->exclui_categoria($_GET['codigo']);
			
        }else{
			$PAGE_CATEGORIAS_RECEITAS->lista_categorias_receitas();
			
        }
			
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_rec'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">RECEITAS</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_RECEITAS->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_RECEITAS->insere_receita($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_nov = $PAGE_RECEITAS->get_receita($_GET['codigo']);
			$PAGE_RECEITAS->imprime_form($reg_nov, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_RECEITAS->atualiza_receita($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_RECEITAS->exclui_receita($_GET['codigo']);
			
        }else{
			$PAGE_RECEITAS->lista_receitas();
			
        }
			
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_cli'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">CLIENTES</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_CLIENTES->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_CLIENTES->insere_cliente($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_cli = $PAGE_CLIENTES->get_cliente($_GET['codigo']);
			$PAGE_CLIENTES->imprime_form($reg_cli, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_CLIENTES->atualiza_cliente($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_CLIENTES->exclui_cliente($_GET['codigo']);
			
        }else{
			$PAGE_CLIENTES->lista_clientes();
			
        }
			
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_cat'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">CATEGORIAS</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_CATEGORIAS->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_CATEGORIAS->insere_categoria($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_nov = $PAGE_CATEGORIAS->get_categoria($_GET['codigo']);
			$PAGE_CATEGORIAS->imprime_form($reg_nov, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_CATEGORIAS->atualiza_categoria($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_CATEGORIAS->exclui_categoria($_GET['codigo']);
			
        }else{
			$PAGE_CATEGORIAS->lista_categorias();
			
        }
			
  }else if($PAGE_TEXTOS->testa_modulos('ADMIN') && $_GET['menu'] == 'cad_pro'){
	   echo'
  <div id="titulos">
    <span style="margin-left:10px;">PRODUTOS</span>
  </div>';
  	  	
		if($_GET['acao'] == "novo"){
			$PAGE_PRODUTOS->imprime_form();
			
		}elseif($_GET['inserir']){
			$PAGE_PRODUTOS->insere_produto($_POST);
			
		}elseif($_GET['acao'] == "edit" && is_numeric($_GET['codigo'])){
			$reg_nov = $PAGE_PRODUTOS->get_produto($_GET['codigo']);
			$PAGE_PRODUTOS->imprime_form($reg_nov, true);
			
		}elseif($_GET['atualizar']){
	  		$PAGE_PRODUTOS->atualiza_produto($_POST);
			
	    }elseif($_GET['acao'] == 'excl' && is_numeric($_GET['codigo'])){
	  		$PAGE_PRODUTOS->exclui_produto($_GET['codigo']);
			
        }else{
			$PAGE_PRODUTOS->lista_produtos();
			
        }
			
  }else{
	  echo'
  <div id="titulos">
    <span style="margin-left:10px;">BEM VINDO</span>
  </div>
  <table width="100%" cellpadding="10" cellspacing="10">
   <tr>
     <td colspan="2"></td>
   </tr>
 </table>';
  	  	  
  }
  
?>  
<?=$PAGE_TEXTOS->rodape();?>
<?=$PAGE_TEXTOS->imprime_rodape();?>
