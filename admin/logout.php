<?php
require "../path.php";
  
$PAGE = new body;
$PAGE->conecta_banco();
  
unset($_SESSION['USUARIO_ID']);
unset($_SESSION['NIVEL_USUARIO']);
unset($_SESSION['USUARIO_NOME']);

session_destroy();
	  
header('location: '.URLADMIN);
?>